Scrum.ai
========

| branch    | status |
|-----------|--------|
| master    |![build](https://gitlab.com/PPL2018csui/Kelas-C/PPL2018-C1/badges/master/build.svg) ![coverage](https://gitlab.com/PPL2018csui/Kelas-C/PPL2018-C1/badges/master/coverage.svg)|
| sit_uat   | ![build](https://gitlab.com/PPL2018csui/Kelas-C/PPL2018-C1/badges/sit_uat/build.svg) ![coverage](https://gitlab.com/PPL2018csui/Kelas-C/PPL2018-C1/badges/sit_uat/coverage.svg)       |
| coba_coba | ![build](https://gitlab.com/PPL2018csui/Kelas-C/PPL2018-C1/badges/coba_coba/build.svg) ![coverage](https://gitlab.com/PPL2018csui/Kelas-C/PPL2018-C1/badges/coba_coba/coverage.svg)       |

## Installing

```
git clone https://gitlab.com/PPL2018csui/Kelas-C/PPL2018-C1
cd PPL2018-C1/scrum-bot
npm install
```

- Copy .env.example to .env (`cp .env.example .env`)
- Setup your DATABASE_URL to your corresponding Postgres connection URL

```
npm run migrate
npm run seed
```


## Running Test

```
npm run test
```


## Building and Serving

```
npm run build
npm run serve
```


## Deployment

```
git clone https://gitlab.com/PPL2018csui/Kelas-C/PPL2018-C1
cd PPL2018-C1/scrum-bot
git init
heroku create
git push heroku master
```

Selanjutnya buat Slack App dan pastikan permission untuk incoming webhook dan userbot sudah ada. Pada Heroku app yang dibuat, silakan konfigurasi environment variable berikut:
- SLACK_VERIFICATION_TOKEN
- SLACK_CLIENT_ID
- SLACK_CLIENT_SECRET
- DATABASE_URL

Integrasi PostgreSQL pada Heroku app yang dibuat melalui Heroku Addons, lalu masukkan url database tersebut ke environment variable DATABASE_URL.

Setelah berjalan, buka alamat Heroku App yang dibuat, lalu klik tombol **Add to Slack**. Kemudian ikuti langkahnya hingga bisa menambah bot ke channel yang diinginkan.

Selanjutnya copy variable user_bot_token ke konfigurasi environment variable Heroku:
- SLACK_USER_BOT_TOKEN
