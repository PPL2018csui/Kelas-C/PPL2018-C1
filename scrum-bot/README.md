scrum-bot
=========

## Setup

```
# Copy then edit .env file
cp .env.example .env

npm install
```


## Running Test

```
npm run test
```


## Migrations

```
# Run
npm run migrate
# Generate
npm run migrations:generate -n {{MigrationName}}
```
