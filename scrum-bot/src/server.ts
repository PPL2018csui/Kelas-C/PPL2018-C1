require('dotenv').config();
require('nunjucks').configure('src/views');

import { createConnection, Connection } from 'typeorm';
import app from './app';

async function setup() {
    const connection: Connection = await createConnection();
    const port = process.env.PORT;
    const server = app.listen(port);
    console.info(`Listening to http://localhost:${port} 🚀`);
}

export = setup();
