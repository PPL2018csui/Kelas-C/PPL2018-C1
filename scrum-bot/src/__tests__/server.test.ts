import request from 'supertest';
import app from '../app';
import { getConnection } from 'typeorm';

const mockListen = jest.fn();
app.listen = mockListen;

afterEach(() => {
    mockListen.mockReset();
    const connection = getConnection();
    connection.close();
});

test('Server works', async () => {
    await require('../server');
    expect(mockListen.mock.calls.length).toBe(1);
    expect(mockListen.mock.calls[0][0]).toBe(process.env.PORT);
});

