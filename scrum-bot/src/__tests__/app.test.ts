import request from 'supertest';
import app from '../app';

test('Slack add button works', async () => {
    require('nunjucks').configure('./src/views');
    const response = await request(app.callback()).get('/');
    expect(response.status).toBe(200);
});
