import { EntryItem } from 'fast-glob/out/types/entries';

jest.setTimeout(15000);

describe('seeder', () => {
    beforeEach(() => {
        jest.resetModules();
        jest.restoreAllMocks();
    });

    it('seeds seeder', async () => {
        const BaseSeeder = require('../seeder/BaseSeeder');
        const mockRunSeeder = jest.fn();
        BaseSeeder.runSeeder = mockRunSeeder;
        const fg = require('fast-glob');

        const seederFiles: EntryItem[] = await fg.async('**/seeder/*.seed.ts');
        fg.async = jest.fn().mockReturnValue(seederFiles);
        const seeder = await require('../seeder');

        expect(mockRunSeeder).toHaveBeenCalledTimes(seederFiles.length);
    });
});