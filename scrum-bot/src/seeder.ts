import * as fg from 'fast-glob';
import { runSeeder } from './seeder/BaseSeeder';
import * as path from 'path';
import { EntryItem } from 'fast-glob/out/types/entries';
import { Connection, createConnection } from 'typeorm';

async function seed() {
    const seederFiles: EntryItem[] = await fg.async('build/seeder/*.seed.js');
    const connection: Connection = await createConnection();
    await connection.synchronize();
    for (const seederFile of seederFiles) {
        const Seeder = require(path.resolve(seederFile.toString()));
        const seeder = new Seeder();
        await runSeeder(seeder);
    }

    await connection.close();
}

export = seed();
