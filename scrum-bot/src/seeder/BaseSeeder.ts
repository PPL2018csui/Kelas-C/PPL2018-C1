export class BaseSeeder {
    shouldRun(): Promise<boolean> {
        return Promise.resolve(true);
    }

    async run(): Promise<void> {}
}

export async function runSeeder(seeder: BaseSeeder) {
    if (process.env.NODE_ENV !== 'production') {
        const shouldRun = await seeder.shouldRun();
        if (shouldRun) {
            await seeder.run();
        }
    }
}
