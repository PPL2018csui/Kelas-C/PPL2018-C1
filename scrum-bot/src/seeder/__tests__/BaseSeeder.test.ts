import { BaseSeeder, runSeeder } from '../BaseSeeder';

describe('BaseSeeder Class', () => {
    it('have the properties shouldRun and must implement run()', () => {
        const example = new BaseSeeder();
        expect(example).toHaveProperty('shouldRun');
        expect(example).toHaveProperty('run');
    });

    it('shouldRun defaults to resolves true', async () => {
        const example = new BaseSeeder();
        await expect(example.shouldRun()).resolves.toBe(true);
    });

    it('run() defaults to a promise which resolves', async () => {
        const example = new BaseSeeder();
        await expect(example.run()).resolves.toBe(undefined);
    });

    it('run() can rejects if it counters errors', async () => {
        class FailingSeeder extends BaseSeeder {
            async run() { return Promise.reject(new Error('fails')); }
        }
        const example = new FailingSeeder();
        await expect(example.run()).rejects.toThrow('fails');
    });
});

describe('runSeeder', () => {
    const oldEnv = process.env;
    const mockRun = jest.fn();
    class MockSeeder extends BaseSeeder {
        async run() { await mockRun(); super.run(); }
    }

    beforeEach(() => {
        jest.resetModules();
        process.env = { ...oldEnv };
    });

    afterEach(() => {
        mockRun.mockReset();
        process.env = oldEnv;
    });

    it('triggers the seeder run() function on non-production environment', async () => {
        const mockSeeder = new MockSeeder();
        process.env.NODE_ENV = 'development';
        await runSeeder(mockSeeder);
        expect(mockRun).toHaveBeenCalledTimes(1);
    });

    it('don\'t triggers the seeder run() function on production environment', async () => {
        const mockSeeder = new MockSeeder();
        process.env.NODE_ENV = 'production';
        await runSeeder(mockSeeder);
        expect(mockRun).toHaveBeenCalledTimes(0);
    });

    it('don\'t triggers the seeder run() function when shouldRun() resolves to false', async () => {
        class NoRunMockSeeder extends MockSeeder {
            async shouldRun() { return Promise.resolve(false); }
        }
        const mockSeeder = new NoRunMockSeeder();
        process.env.NODE_ENV = 'development';
        await runSeeder(mockSeeder);
        expect(mockRun).toHaveBeenCalledTimes(0);
    });
});
