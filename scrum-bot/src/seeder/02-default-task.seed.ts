import { BaseSeeder } from './BaseSeeder';
import { Task } from '../entity/Task';
import { List, ListType } from '../entity/List';

class DefaultTaskSeeder extends BaseSeeder {
    async shouldRun() {
        const existing = await Task.findOne({ title: `Default task 0 for ${ListType[0]} List at Default Project` });
        return existing === undefined;
    }

    async run() {
        const lists = await List.find({ relations: ['project'] });

        lists.forEach(async list => {
            const numOfDummyTask = 3;
            for (let taskIdx = 0; taskIdx < numOfDummyTask; taskIdx++) {
                const task = new Task();
                task.title = `Default task ${taskIdx} for ${ListType[list.name]} List at ${list.project.name}`;
                task.list = list;
                task.point = 2;
                await task.save();
            }
        });
    }
}

export = DefaultTaskSeeder;
