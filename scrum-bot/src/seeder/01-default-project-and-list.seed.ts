import { BaseSeeder } from './BaseSeeder';
import { Project } from '../entity/Project';
import { List, ListType } from '../entity/List';

class DefaultProjectListSeeder extends BaseSeeder {
    async shouldRun() {
        const existing = await Project.findOne({ name: 'Default Project' });
        return existing === undefined;
    }

    async run() {
        const project = new Project();
        project.name = 'Default Project';
        await project.save();

        for (const listType in ListType) {
            if (!isNaN(Number(listType))) {
                const list = new List();
                list.name = Number(listType);
                list.project = project;
                await list.save();
            }
        }

    }
}

export = DefaultProjectListSeeder;
