import { Entity, Column } from 'typeorm';
import { DefaultEntity } from './DefaultEntity';

export enum BlockerStatus {
    Solved,
    Pending,
    Hold
}

@Entity()
export class Blocker extends DefaultEntity {
    @Column('varchar') user!: string;

    @Column('text') text!: string;

    @Column('int') status: BlockerStatus = BlockerStatus.Pending;
}
