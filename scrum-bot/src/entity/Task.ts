import { Entity, Column, ManyToOne, OneToMany } from 'typeorm';
import { List } from './List';
import { TaskMutation } from './TaskMutation';
import { DefaultEntity } from './DefaultEntity';
import { Sprint } from './Sprint';

@Entity()
export class Task extends DefaultEntity {
    @Column('varchar') title!: string;

    @Column('int', { nullable: true })
    point!: number;

    @Column('varchar', { nullable: true })
    assignee!: string;

    @ManyToOne(type => List, list => list.tasks)
    list!: List;

    @OneToMany(type => TaskMutation, mutation => mutation.task)
    mutations!: TaskMutation[];

    @ManyToOne(type => Sprint, sprint => sprint.tasks)
    sprint!: Sprint;
}
