import {
    createConnection,
    getRepository,
    getConnection,
    getManager
} from 'typeorm';
import { TaskMutation } from '../TaskMutation';
import { Task } from '../Task';
import { List, ListType } from '../List';

beforeAll(async () => {
    const connection = await createConnection();
});

beforeEach(async () => {
    await getConnection().synchronize(true);
});

afterAll(async () => {
    await getConnection().close();
});

test('TaskMutation entity', async () => {
    const manager = getManager();
    const list1 = new List();
    list1.name = ListType.Todo;
    const list2 = new List();
    list2.name = ListType.Doing;
    await manager.save(List, [list1, list2]);

    const task = new Task();
    task.title = 'asd';
    task.point = 1;
    await manager.save(Task, task);

    const mutation = new TaskMutation();
    mutation.task = task;
    mutation.before = list1;
    mutation.after = list2;
    await manager.save(TaskMutation, mutation);

    const retrieved = await manager.findOneById(TaskMutation, mutation.id, {
        relations: ['task', 'before', 'after']
    });

    expect(retrieved!.task.id).toBe(task.id);
    expect(retrieved!.before.id).toBe(list1.id);
    expect(retrieved!.after.id).toBe(list2.id);
});
