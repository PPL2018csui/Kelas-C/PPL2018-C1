import { createConnection, getRepository, getConnection } from 'typeorm';
import { Project } from '../Project';

beforeAll(async () => {
    const connection = await createConnection();
});

beforeEach(async () => {
    await getConnection().synchronize(true);
});

afterAll(async () => {
    await getConnection().close();
});

test('Project entity', async () => {
    const repository = getRepository(Project);
    const project = new Project();
    project.name = 'Lorem Ipsum';
    const savedProject = await repository.save(project);
    expect(savedProject).toBeTruthy();
    const retrieved = await repository.findOneById(savedProject.id);
    expect(retrieved!).toBeDefined();
    expect(retrieved!.name).toBe('Lorem Ipsum');
});

test('Project name must be defined', async () => {
    const repository = getRepository(Project);
    const project = new Project();
    await expect(repository.save(project)).rejects.toMatchSnapshot();
});
