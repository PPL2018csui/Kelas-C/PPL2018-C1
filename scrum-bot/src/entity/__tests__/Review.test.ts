import { getConnection, createConnection, getRepository } from 'typeorm';
import { Review, ReviewType } from '../Review';

beforeAll(async () => {
    const connection = await createConnection();
});

beforeEach(async () => {
    await getConnection().synchronize(true);
});

afterAll(async () => {
    await getConnection().close();
});

test('Review entity exist', async () => {
    expect(Review).toBeDefined;
});

test('Review entity', async () => {
    const repository = getRepository(Review);
    const review = new Review();
    review.text = 'test';
    review.type = ReviewType.Green;
    const savedReview = await repository.save(review);
    expect(savedReview).toBeTruthy();
    const retrieved = await repository.findOneById(savedReview.id);
    expect(retrieved).toBeDefined();
    expect(retrieved!.text).toBe('test');
    expect(retrieved!.type).toBe(ReviewType.Green);
});

test('some Review attributes must be defined', async () => {
    const repository = getRepository(Review);
    const review = new Review();
    await expect(repository.save(review)).rejects.toMatchSnapshot();
});
