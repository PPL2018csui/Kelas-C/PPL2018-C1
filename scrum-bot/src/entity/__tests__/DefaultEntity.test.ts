import { DefaultEntity } from '../DefaultEntity';

class SampleBaseEntity extends DefaultEntity {
    // empty
}

describe('Base', () => {
    it('can be extended', () => {
        expect(typeof SampleBaseEntity).toBe('function');
    });

    describe('instance', () => {
        it('attributes can be set', () => {
            const entity = new SampleBaseEntity();
            entity.id = '1';
            entity.createdAt = new Date();
            entity.updatedAt = new Date();
            expect(entity).toBeInstanceOf(SampleBaseEntity);
            expect(entity).toHaveProperty('id');
            expect(entity).toHaveProperty('createdAt');
            expect(entity).toHaveProperty('updatedAt');
        });

        it('implements active record', () => {
            expect(DefaultEntity.find).toBeDefined();
            expect(DefaultEntity.findOne).toBeDefined();
            expect(DefaultEntity.findOneById).toBeDefined();
            expect(DefaultEntity.update).toBeDefined();
            expect(DefaultEntity.updateById).toBeDefined();
            expect(DefaultEntity.remove).toBeDefined();
            expect(DefaultEntity.removeById).toBeDefined();
            const entity = new SampleBaseEntity();
            expect(entity.save).toBeDefined();
            expect(entity.remove).toBeDefined();
        });
    });
});
