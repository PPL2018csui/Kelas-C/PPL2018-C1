import {
    createConnection,
    getRepository,
    getConnection,
    getManager
} from 'typeorm';
import { Task } from '../Task';
import { List, ListType } from '../List';

beforeAll(async () => {
    const connection = await createConnection();
});

beforeEach(async () => {
    await getConnection().synchronize(true);
});

afterAll(async () => {
    await getConnection().close();
});

test('Task entity', async () => {
    const repository = getRepository(Task);
    const task = new Task();
    task.title = 'asd';
    task.point = 1;
    const saved = await repository.save(task);
    expect(saved).toBeTruthy();
    const retrieved = await repository.findOneById(saved.id);
    expect(retrieved!).toBeDefined();
    expect(retrieved!.title).toBe('asd');
});

test('Some Task attributes must be defined', async () => {
    const repository = getRepository(Task);
    const task = new Task();
    await expect(repository.save(task)).rejects.toMatchSnapshot();
});

test('Task - List relation', async () => {
    const manager = getManager();
    const list = new List();
    list.name = ListType.Todo;
    await manager.save(list);

    const task = new Task();
    task.title = 'asd';
    task.point = 1;
    task.list = list;
    await manager.save(task);

    const retrievedTask = await manager.findOneById(Task, task.id, {
        relations: ['list']
    });
    const retrievedList = await manager.findOneById(List, list.id, {
        relations: ['tasks']
    });

    expect(retrievedTask!.list).toBeDefined();
    expect(retrievedTask!.list.id).toBe(list.id);
    expect(retrievedList!.tasks).toHaveLength(1);
    expect(retrievedList!.tasks[0].id).toBe(task.id);
});
