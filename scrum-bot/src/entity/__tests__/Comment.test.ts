import { createConnection, getRepository, getConnection } from 'typeorm';
import { Comment } from '../Comment';

beforeAll(async () => {
    const connection = await createConnection();
});

beforeEach(async () => {
    await getConnection().synchronize(true);
});

afterAll(async () => {
    await getConnection().close();
});

test('Comment entity', async () => {
    const repository = getRepository(Comment);
    const comment = new Comment();
    comment.from = 'asd';
    comment.to = 'asd';
    comment.message = 'asd';
    const saved = await repository.save(comment);
    expect(saved).toBeTruthy();
    const retrieved = await repository.findOneById(saved.id);
    expect(retrieved!).toBeDefined();
    expect(retrieved!.from).toBe('asd');
    expect(retrieved!.to).toBe('asd');
    expect(retrieved!.message).toBe('asd');
});

test('Comment name must be defined', async () => {
    const repository = getRepository(Comment);
    const comment = new Comment();
    await expect(repository.save(comment)).rejects.toMatchSnapshot();
});
