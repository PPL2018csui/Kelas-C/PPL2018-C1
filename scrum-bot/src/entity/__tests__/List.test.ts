import {
    createConnection,
    getRepository,
    getConnection,
    getConnectionManager,
    getManager
} from 'typeorm';
import { List, ListType } from '../List';
import { Project } from '../Project';

beforeAll(async () => {
    const connection = await createConnection();
});

beforeEach(async () => {
    await getConnection().synchronize(true);
});

afterAll(async () => {
    await getConnection().close();
});

test('List entity', async () => {
    const repository = getRepository(List);
    const list = new List();
    list.name = ListType.Todo;
    const savedList = await repository.save(list);
    expect(savedList).toBeTruthy();
    const retrieved = await repository.findOneById(savedList.id);
    expect(retrieved!).toBeDefined();
    expect(retrieved!.name).toBe(list.name);
});

test('List - Project relation', async () => {
    const manager = getManager();
    const project = new Project();
    project.name = 'asd';
    await manager.save(project);

    const list = new List();
    list.name = ListType.Todo;
    list.project = project;
    await manager.save(list);

    const retrievedList = await manager.findOneById(List, list.id, {
        relations: ['project']
    });
    const retrievedProject = await manager.findOneById(Project, project.id, {
        relations: ['lists']
    });

    expect(retrievedList!.project).toBeDefined();
    expect(retrievedList!.project.id).toBe(project.id);
    expect(retrievedProject!.lists).toHaveLength(1);
    expect(retrievedProject!.lists[0].id).toBe(list.id);
});
