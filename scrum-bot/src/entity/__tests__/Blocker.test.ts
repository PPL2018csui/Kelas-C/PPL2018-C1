import { createConnection, getRepository, getConnection } from 'typeorm';
import { Blocker, BlockerStatus } from '../Blocker';

beforeAll(async () => {
    const connection = await createConnection();
});

beforeEach(async () => {
    await getConnection().synchronize(true);
});

afterAll(async () => {
    await getConnection().close();
});

test('Blocker entity', async () => {
    const repository = getRepository(Blocker);
    const blocker = new Blocker();
    blocker.text = 'asd';
    blocker.user = 'asd';
    const savedBlocker = await repository.save(blocker);
    expect(savedBlocker).toBeTruthy();
    const retrieved = await repository.findOneById(savedBlocker.id);
    expect(retrieved!).toBeDefined();
    expect(retrieved!.text).toBe('asd');
    expect(retrieved!.user).toBe('asd');
    expect(retrieved!.status).toBe(BlockerStatus.Pending);
});

test('some Blocker attributes must be defined', async () => {
    const repository = getRepository(Blocker);
    const blocker = new Blocker();
    await expect(repository.save(blocker)).rejects.toMatchSnapshot();
});
