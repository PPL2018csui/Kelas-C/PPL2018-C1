import { createConnection, getRepository, getConnection } from 'typeorm';
import { Sprint } from '../Sprint';
import { Project } from '../Project';
import { Task } from '../Task';

beforeAll(async () => {
    const connection = await createConnection();
});

beforeEach(async () => {
    await getConnection().synchronize(true);
});

afterAll(async () => {
    await getConnection().close();
});

test('Sprint entity', async () => {
    const sprint = new Sprint();
    sprint.name = 'Sprint 1';
    const saved = await sprint.save();
    expect(saved.hasId()).toBe(true);

    const retrieved = await Sprint.findOneById(saved.id);
    expect(retrieved).toBeDefined();
    expect(retrieved!.name).toBe(sprint.name);
});

test('Sprint name must be defined', async () => {
    const sprint = new Sprint();
    await expect(sprint.save()).rejects.toMatchSnapshot();
});

test('Sprint - Project relation', async () => {
    const project = new Project();
    project.name = 'Project 1';
    await project.save();

    const sprint = new Sprint();
    sprint.name = 'Sprint 1';
    sprint.project = project;
    sprint.active = true;
    await sprint.save();

    expect(await project.getActiveSprint()).toBeDefined();
    expect((await project.getActiveSprint())!.id).toBe(sprint.id);

    const retrievedProject = await Project.findOneById(project.id, { relations: ['sprints'] });
    const retrievedSprint = await Sprint.findOneById(sprint.id, { relations: ['project'] });

    expect(retrievedProject!.sprints).toBeDefined();
    expect(retrievedProject!.sprints!.length).toBe(1);
    expect(retrievedProject!.sprints[0].id).toBe(sprint.id);

    expect(retrievedSprint!.project).toBeDefined();
    expect(retrievedSprint!.project.id).toBe(project.id);

    await sprint.remove();
    expect(await project.getActiveSprint()).not.toBeDefined();
});


test('Sprint - Task relation', async () => {
    const sprint = new Sprint();
    sprint.name = 'Sprint 1';
    await sprint.save();

    const task = new Task();
    task.title = 'Task 1';
    task.sprint = sprint;
    task.point = 1;
    await task.save();

    const retrievedSprint = await Sprint.findOneById(sprint.id, { relations: ['tasks'] });
    const retrievedTask = await Task.findOneById(task.id, { relations: ['sprint'] });

    expect(retrievedSprint!.tasks).toBeDefined();
    expect(retrievedSprint!.tasks.length).toBe(1);
    expect(retrievedSprint!.tasks[0].id).toBe(task.id);

    expect(retrievedTask!.sprint).toBeDefined();
    expect(retrievedTask!.sprint.id).toBe(sprint.id);
});

