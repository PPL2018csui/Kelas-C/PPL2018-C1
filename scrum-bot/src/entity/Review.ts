import { Entity, Column } from 'typeorm';
import { DefaultEntity } from './DefaultEntity';

export enum ReviewType {
    Red,
    Green
}

@Entity()
export class Review extends DefaultEntity {
    @Column('varchar') text!: string;

    @Column('int') type: ReviewType = ReviewType.Red;
}
