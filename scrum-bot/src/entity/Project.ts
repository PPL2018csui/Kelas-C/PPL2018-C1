import { Entity, Column, OneToMany } from 'typeorm';
import { List } from './List';
import { DefaultEntity } from './DefaultEntity';
import { Sprint } from './Sprint';

@Entity()
export class Project extends DefaultEntity {
    @Column('varchar') name!: string;

    @OneToMany(type => List, list => list.project)
    lists!: List[];

    @OneToMany(type => Sprint, sprint => sprint.project)
    sprints!: Sprint[];

    async getActiveSprint(): Promise<Sprint | undefined> {
        const sprint = await Sprint.createQueryBuilder('sprint')
            .where('sprint.active = true')
            .andWhere('sprint.project = :projectId', { projectId: this.id })
            .orderBy('sprint.createdAt', 'DESC')
            .limit(1)
            .getOne();

        return sprint;
    }
}
