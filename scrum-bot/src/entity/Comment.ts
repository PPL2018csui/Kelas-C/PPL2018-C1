import { Entity, Column, ManyToOne, OneToMany } from 'typeorm';
import { DefaultEntity } from './DefaultEntity';

@Entity()
export class Comment extends DefaultEntity {
    @Column('varchar') from!: string;

    @Column('varchar') to!: string;

    @Column('text') message!: string;
}
