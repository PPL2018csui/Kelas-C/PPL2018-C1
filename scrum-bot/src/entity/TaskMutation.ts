import { Entity, ManyToOne } from 'typeorm';
import { Task } from './Task';
import { List } from './List';
import { DefaultEntity } from './DefaultEntity';

@Entity()
export class TaskMutation extends DefaultEntity {
    @ManyToOne(type => Task, task => task.mutations)
    task!: Task;

    @ManyToOne(type => List)
    before!: List;

    @ManyToOne(type => List)
    after!: List;
}
