import { Entity, Column, ManyToOne, OneToMany } from 'typeorm';
import { Project } from './Project';
import { Task } from './Task';
import { DefaultEntity } from './DefaultEntity';

export enum ListType {
    Backlog,
    Todo,
    Doing,
    Done
}

@Entity()
export class List extends DefaultEntity {
    @Column('int') name: ListType = ListType.Backlog;

    @ManyToOne(type => Project, project => project.lists)
    project!: Project;

    @OneToMany(type => Task, task => task.list)
    tasks!: Task[];
}
