import { Column, OneToMany, ManyToOne, Entity } from 'typeorm';
import { DefaultEntity } from './DefaultEntity';
import { Task } from './Task';
import { Project } from './Project';

@Entity()
export class Sprint extends DefaultEntity {
    @Column('varchar')
    name!: string;

    @Column('boolean')
    active: boolean = false;

    @OneToMany(type => Task, task => task.sprint)
    tasks!: Task[];

    @ManyToOne(type => Project, project => project.sprints)
    project!: Project;
}
