import Koa from 'koa';
import Router from 'koa-router';
import bodyParser from 'koa-bodyparser';
import { render } from 'nunjucks';
import { router as slackRouter } from './slack/routes';

const app = new Koa();
const router = new Router();

app.use(bodyParser());

router.get('/', async (ctx: Koa.Context) => {
    ctx.body = await render('index.html', {
        client_id: process.env.SLACK_CLIENT_ID
    });
});


app.use(router.routes());
app.use(slackRouter.routes());

export = app;
