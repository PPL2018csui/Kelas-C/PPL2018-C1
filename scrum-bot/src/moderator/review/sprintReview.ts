import { MessageEventType } from '../../slack/utils';
import { sendMessageToChannel } from '../../slack';
import { Review, ReviewType } from '../../entity/Review';

export async function redSprintReview(event: MessageEventType) {
    const regexToken = /^\/sprint_review_red\s(.+)$/.exec(event.text);

    if (regexToken == null) {
        sendMessageToChannel(event.channel, 'Usage: /sprint_review_red [message]');
        return;
    }

    const review = new Review();
    review.text = regexToken[1];
    review.type = ReviewType.Red;

    await review.save();
    sendMessageToChannel(event.channel, 'Review berhasil ditambahkan! Ada lagi?');
}

export async function greenSprintReview(event: MessageEventType) {
    const regexToken = /^\/sprint_review_green\s(.+)$/.exec(event.text);

    if (regexToken == null) {
        sendMessageToChannel(event.channel, 'Usage: /sprint_review_green [message]');
        return;
    }

    const review = new Review();
    review.text = regexToken[1];
    review.type = ReviewType.Green;

    await review.save();
    sendMessageToChannel(event.channel, 'Review berhasil ditambahkan! Ada lagi?');
}

export async function showSprintReview(event: MessageEventType) {
    if (!/^\/sprint_review_show\s*$/.test(event.text)) {
        sendMessageToChannel(event.channel, 'Usage: /sprint_review_show');
        return;
    }

    const redReview = await Review.find({ type: ReviewType.Red });
    const greenReview = await Review.find({ type: ReviewType.Green });

    if (redReview.length === 0 && greenReview.length === 0) {
        sendMessageToChannel(event.channel, 'Tidak ada summary sprint review untuk ditampilkan!');
        return;
    }

    let msg: string = 'Summary:';

    if (redReview.length > 0) {
        msg += '\n\nHal yang kurang di sprint ini:';
        redReview.forEach((r) => {
            msg += `\n- ${r.text}`;
        });
    }

    if (greenReview.length > 0) {
        msg += '\n\nHal yang baik di sprint ini:';
        greenReview.forEach((r) => {
            msg += `\n- ${r.text}`;
        });
    }

    sendMessageToChannel(event.channel, msg);
}
