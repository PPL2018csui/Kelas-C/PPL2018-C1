import { redSprintReview, greenSprintReview, showSprintReview } from '../sprintReview';
import { MessageOrigin, MessageEventType } from '../../../slack/utils';
import { ReviewType } from '../../../entity/Review';

describe('redSprintReview', () => {
    const baseEvent: MessageEventType = {
        type: 'message',
        channel: 'C2147483705',
        user: 'U2147483697',
        text: '/sprint_review_red text',
        ts: '1355517523.000005',
        origin: MessageOrigin.channels
    };

    it('is defined', () => {
        expect(redSprintReview).toBeDefined();
    });

    it('saves red review', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const reviewEntity = require('../../../entity/Review');
        const mockReviewSave = jest.fn();
        const mockReviewConstructor = jest.fn().mockImplementation(() => ({
            save: mockReviewSave,
        }));
        reviewEntity.Review = mockReviewConstructor;

        const event = baseEvent;
        const message = 'Review berhasil ditambahkan! Ada lagi?';

        await redSprintReview(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
        expect(mockReviewConstructor).toBeCalledWith();
        expect(mockReviewSave).toBeCalledWith();
    });

    it('rejects badly formatted command', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/sprint_review_redX'
        };
        const message = 'Usage: /sprint_review_red [message]';

        await redSprintReview(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
    });

    it('rejects badly formatted command (2)', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/sprint_review_red '
        };
        const message = 'Usage: /sprint_review_red [message]';

        await redSprintReview(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
    });
});

describe('greenSprintReview', () => {
    const baseEvent: MessageEventType = {
        type: 'message',
        channel: 'C2147483705',
        user: 'U2147483697',
        text: '/sprint_review_green text',
        ts: '1355517523.000005',
        origin: MessageOrigin.channels
    };

    it('is defined', () => {
        expect(greenSprintReview).toBeDefined();
    });

    it('saves green review', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const reviewEntity = require('../../../entity/Review');
        const mockReviewSave = jest.fn();
        const mockReviewConstructor = jest.fn().mockImplementation(() => ({
            save: mockReviewSave,
        }));
        reviewEntity.Review = mockReviewConstructor;

        const event = baseEvent;
        const message = 'Review berhasil ditambahkan! Ada lagi?';

        await greenSprintReview(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
        expect(mockReviewConstructor).toBeCalledWith();
        expect(mockReviewSave).toBeCalledWith();
    });

    it('rejects badly formatted command', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/sprint_review_greenX'
        };
        const message = 'Usage: /sprint_review_green [message]';

        await greenSprintReview(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
    });

    it('rejects badly formatted command (2)', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/sprint_review_green '
        };
        const message = 'Usage: /sprint_review_green [message]';

        await greenSprintReview(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
    });
});

describe('showSprintReview', () => {
    const baseEvent: MessageEventType = {
        type: 'message',
        channel: 'C2147483705',
        user: 'U2147483697',
        text: '/sprint_review_show',
        ts: '1355517523.000005',
        origin: MessageOrigin.channels
    };

    it('is defined', () => {
        expect(showSprintReview).toBeDefined();
    });

    it('shows sprint review summary', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const reviewEntity = require('../../../entity/Review');
        const mockReviewFind = jest.fn();
        mockReviewFind.mockReturnValueOnce([
            { text: 'a' },
            { text: 'b' },
            { text: 'c' }
        ]).mockReturnValueOnce([
            { text: 'd' },
            { text: 'e' },
            { text: 'f' }
        ]);
        reviewEntity.Review.find = mockReviewFind;

        const event = baseEvent;
        const message = `Summary:\n\nHal yang kurang di sprint ini:\n- a\n- b\n- c\n\nHal yang baik di sprint ini:\n- d\n- e\n- f`;

        await showSprintReview(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
        expect(mockReviewFind).toBeCalledWith({ type: ReviewType.Red });
        expect(mockReviewFind).toBeCalledWith({ type: ReviewType.Green });
    });

    it('rejects badly formatted', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/sprint_review_showX'
        };
        const message = 'Usage: /sprint_review_show';

        await showSprintReview(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
    });

    it('shows only red sprint review summary', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const reviewEntity = require('../../../entity/Review');
        const mockReviewFind = jest.fn();
        mockReviewFind.mockReturnValueOnce([
            { text: 'a' },
            { text: 'b' },
            { text: 'c' }
        ]).mockReturnValueOnce([]);
        reviewEntity.Review.find = mockReviewFind;

        const event = baseEvent;
        const message = `Summary:\n\nHal yang kurang di sprint ini:\n- a\n- b\n- c`;

        await showSprintReview(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
        expect(mockReviewFind).toBeCalledWith({ type: ReviewType.Red });
        expect(mockReviewFind).toBeCalledWith({ type: ReviewType.Green });
    });

    it('shows only green sprint review summary', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const reviewEntity = require('../../../entity/Review');
        const mockReviewFind = jest.fn();
        mockReviewFind.mockReturnValueOnce([]).mockReturnValueOnce([
            { text: 'a' },
            { text: 'b' },
            { text: 'c' }
        ]);
        reviewEntity.Review.find = mockReviewFind;

        const event = baseEvent;
        const message = `Summary:\n\nHal yang baik di sprint ini:\n- a\n- b\n- c`;

        await showSprintReview(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
        expect(mockReviewFind).toBeCalledWith({ type: ReviewType.Red });
        expect(mockReviewFind).toBeCalledWith({ type: ReviewType.Green });
    });

    it('shows no sprint review summary available', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const reviewEntity = require('../../../entity/Review');
        const mockReviewFind = jest.fn();
        mockReviewFind.mockReturnValueOnce([])
            .mockReturnValueOnce([]);
        reviewEntity.Review.find = mockReviewFind;

        const event = baseEvent;
        const message = 'Tidak ada summary sprint review untuk ditampilkan!';

        await showSprintReview(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
        expect(mockReviewFind).toBeCalledWith({ type: ReviewType.Red });
        expect(mockReviewFind).toBeCalledWith({ type: ReviewType.Green });
    });
});
