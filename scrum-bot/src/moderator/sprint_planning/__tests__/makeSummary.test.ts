import { createConnection } from 'typeorm';
import { makeSummary } from '../makeSummary';
import { MessageEventType, MessageOrigin } from '../../../slack/utils';
import { Sprint } from '../../../entity/Sprint';
import { Task } from '../../../entity/Task';
import { sendMessageToChannel } from '../../../slack';


describe('makeSummary', () => {
    const baseEvent: MessageEventType = {
        type: 'message',
        channel: 'C2147483705',
        user: 'U2147483697',
        text: 'Hello world',
        ts: '1355517523.000005',
        origin: MessageOrigin.channels,
    };

    it('is defined', () => {
        expect(makeSummary).toBeDefined();
    });


    it('Make a summary based on sprint planning', async () => {
        const connection = await createConnection();
        await connection.synchronize(true);

        const sprint = new Sprint();
        sprint.name = 'Sprint Planning 1';
        await sprint.save();

        for (let i = 0; i < 2; i++) {
            const task = new Task();
            task.sprint = sprint;
            task.title = 'Task_' + i;
            task.assignee = 'User_' + i;
            task.point = i;
            await task.save();
        }

        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/summarize_planning'
        };

        let message = 'Berikut merupakan summary dari sprint planning untuk sprint Sprint Planning 1 yang telah dilakukan \n';
        let points = 0;
        for (let i = 0; i < 2; i++) {
            message += 'Task_' + i + ' (<@' + 'User_' + i + '>) - Bobot: ' + i + '\n';
            points += i;
        }
        message += 'Total bobot yang diambil adalah ' + points + '\n';
        message += 'Terima kasih atas partisipasinya!';

        await makeSummary(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
        await connection.close();
    }, 10000);

    it('handles unassigned tasks', async () => {
        const connection = await createConnection();
        await connection.synchronize(true);

        const sprint = new Sprint();
        sprint.name = 'Sprint Planning 1';
        await sprint.save();

        const task = new Task();
        task.title = 'Task 1';
        task.point = 1;
        task.sprint = sprint;
        await task.save();

        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/summarize_planning'
        };

        let message = 'Berikut merupakan summary dari sprint planning untuk sprint Sprint Planning 1 yang telah dilakukan \n';
        message += 'Task 1 - Bobot: 1\n';
        message += 'Total bobot yang diambil adalah 1\n';
        message += 'Terima kasih atas partisipasinya!';

        await makeSummary(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
        await connection.close();
    });

    it('There is no task in sprint table', async () => {
        const connection = await createConnection();
        await connection.synchronize(true);

        const sprint = new Sprint();
        sprint.name = 'Sprint Planning 1';
        await sprint.save();

        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(false);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/summarize_planning'
        };
        const message = 'Tidak ada task yang sedang dikerjakan.';

        await makeSummary(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
        await connection.close();
    });

    it('There is no task in list table', async () => {
        const connection = await createConnection();
        await connection.synchronize(true);

        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(false);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/summarize_planning'
        };
        const message = 'Mulai sprint planning terlebih dahulu.';

        await makeSummary(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
        await connection.close();
    });
});