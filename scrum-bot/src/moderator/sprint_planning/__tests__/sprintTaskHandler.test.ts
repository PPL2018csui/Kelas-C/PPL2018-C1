import { MessageEventType, MessageOrigin } from '../../../slack/utils';
import {
    sprintTaskHandler,
    CMD_SPRINT_TASK_PLAN_USAGE
} from '../sprintTaskHandler';
import {
    sprintNotFound,
    taskNotFound,
    taskAssignedToSprint,
    listNotFound
} from '../../chatResponse';
import { ListType } from '../../../entity/List';

const baseEvent: MessageEventType = {
    type: 'message',
    channel: 'C2147483705',
    user: 'U2147483697',
    text: 'Hello world',
    ts: '1355517523.000005',
    origin: MessageOrigin.channels
};

describe('handler', () => {
    it('is defined', () => {
        expect(sprintTaskHandler).toBeDefined();
    });

    it('ignores unknown messages', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: 'asd'
        };
        await sprintTaskHandler(event);
        expect(mockChannel).toHaveBeenCalledTimes(0);
    });
});

describe('/sprint_task_plan', () => {
    it('rejects badly formatted message with instruction', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/sprint_task_plan '
        };
        const message = CMD_SPRINT_TASK_PLAN_USAGE;

        await sprintTaskHandler(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
    });

    it('rejects non-existing sprint', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/sprint_task_plan Sprint 2 | Task 1'
        };

        const message = sprintNotFound('Sprint 2');

        const sprintModule = require('../../../entity/Sprint');
        const taskModule = require('../../../entity/Task');
        const mockSprintFindOne = jest.fn().mockReturnValue(undefined);
        const mockTaskFindOne = jest.fn().mockReturnValue({});
        sprintModule.Sprint.findOne = mockSprintFindOne;
        taskModule.Task.findOne = mockTaskFindOne;

        await sprintTaskHandler(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
        expect(mockSprintFindOne).toBeCalledWith({ name: 'Sprint 2' });
    });

    it('rejects non-existing task', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/sprint_task_plan Sprint 1 | Task 2'
        };

        const message = taskNotFound('Task 2');

        const sprintModule = require('../../../entity/Sprint');
        const taskModule = require('../../../entity/Task');
        const mockSprintFindOne = jest.fn().mockReturnValue({});
        const mockTaskFindOne = jest.fn().mockReturnValue(undefined);
        sprintModule.Sprint.findOne = mockSprintFindOne;
        taskModule.Task.findOne = mockTaskFindOne;

        await sprintTaskHandler(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
        expect(mockTaskFindOne).toBeCalledWith({ title: 'Task 2' });
    });

    it('rejects if todo list not exists', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/sprint_task_plan Sprint 1 | Task 1'
        };

        const message = listNotFound(ListType.Todo);

        const sprintModule = require('../../../entity/Sprint');
        const taskModule = require('../../../entity/Task');
        const listModule = require('../../../entity/List');
        const mockSprintFindOne = jest.fn().mockReturnValue({});
        const mockTaskFindOne = jest.fn().mockReturnValue({});
        const mockListFindOne = jest.fn().mockReturnValue(undefined);
        sprintModule.Sprint.findOne = mockSprintFindOne;
        taskModule.Task.findOne = mockTaskFindOne;
        listModule.List.findOne = mockListFindOne;

        await sprintTaskHandler(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
    });

    it('assigns task to a sprint', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/sprint_task_plan Sprint 1 | Task 1'
        };

        const message = taskAssignedToSprint('Task 1', 'Sprint 1');

        const sprintModule = require('../../../entity/Sprint');
        const taskModule = require('../../../entity/Task');
        const listModule = require('../../../entity/List');
        const mockSave = jest.fn();
        const mockTask = { save: mockSave };
        const mockSprint = { name: 'Sprint 1' };
        const mockListTodo = { name: ListType.Todo };
        const mockSprintSetter = jest.fn();
        const mockListSetter = jest.fn();
        Object.defineProperty(mockTask, 'sprint', {
            set: mockSprintSetter,
        });
        Object.defineProperty(mockTask, 'list', {
            set: mockListSetter,
        });
        const mockSprintFindOne = jest.fn().mockReturnValue(mockSprint);
        const mockTaskFindOne = jest.fn().mockReturnValue(mockTask);
        const mockListFindOne = jest.fn().mockReturnValue(mockListTodo);
        sprintModule.Sprint.findOne = mockSprintFindOne;
        taskModule.Task.findOne = mockTaskFindOne;
        listModule.List.findOne = mockListFindOne;

        await sprintTaskHandler(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
        expect(mockTaskFindOne).toBeCalledWith({ title: 'Task 1' });
        expect(mockSprintFindOne).toBeCalledWith({ name: 'Sprint 1' });
        expect(mockListFindOne).toBeCalledWith({ name: ListType.Todo });
        expect(mockSprintSetter).toBeCalledWith(mockSprint);
        expect(mockListSetter).toBeCalledWith(mockListTodo);
        expect(mockSave).toBeCalled();
    });
});
