import { createConnection, getConnection } from 'typeorm';
import { List, ListType } from '../../../entity/List';
import { MessageEventType, MessageOrigin } from '../../../slack/utils';
import { addTask, CMD_TASK_ADD_USAGE, CMD_TASK_ESTIMATE_USAGE, estimateTask } from '../addTask';
import { listNotFound, taskCreated, taskNotFound, taskEstimated } from '../../chatResponse';
import { Task } from '../../../entity/Task';

describe('addTask', () => {
    const baseEvent: MessageEventType = {
        type: 'message',
        channel: 'C2147483705',
        user: 'U2147483697',
        text: '/task_add Buat handl',
        ts: '1355517523.000005',
        origin: MessageOrigin.channels
    };

    it('rejects badly formatted, case: /task_addsd', async () => {
        const slack = require('../../../slack');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/task_addsdf'
        };
        const message = CMD_TASK_ADD_USAGE;

        await addTask(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
    });

    it('rejects, when list to do not found', async () => {
        const connection = await createConnection();
        await connection.synchronize(true);

        const slack = require('../../../slack');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/task_add buat website'
        };
        const message = listNotFound(ListType.Backlog);

        await addTask(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
        await connection.close();
    });

    it('adds task', async () => {
        const connection = await createConnection();
        await connection.synchronize(true);

        const list = new List();
        list.name = ListType.Backlog;
        await list.save();

        const slack = require('../../../slack');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/task_add buat website'
        };
        const message = taskCreated('buat website');

        await addTask(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
        await connection.close();
    });
});

describe('/task_estimate', () => {
    const baseEvent: MessageEventType = {
        type: 'message',
        channel: 'C2147483705',
        user: 'U2147483697',
        text: '',
        ts: '1355517523.000005',
        origin: MessageOrigin.channels
    };

    it('rejects badly formatted, case: /task_estimated', async () => {
        const slack = require('../../../slack');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/task_estimated'
        };
        const message = CMD_TASK_ESTIMATE_USAGE;

        await estimateTask(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
    });

    it('rejects, when task not found', async () => {
        const connection = await createConnection();
        await connection.synchronize(true);

        const slack = require('../../../slack');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/task_estimate asdasdasd 13'
        };
        const message = taskNotFound('asdasdasd');

        await estimateTask(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
        await connection.close();
    });

    it('updates the estimate of the defined task', async () => {
        const connection = await createConnection();
        await connection.synchronize(true);

        const slack = require('../../../slack');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const task = new Task();
        task.title = 'asd';
        await task.save();

        const event = {
            ...baseEvent,
            text: '/task_estimate asd 13'
        };
        const message = taskEstimated('asd', 13);

        await estimateTask(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
        await connection.close();
    });
});
