import { getManager, createConnection, getConnection } from 'typeorm';
import { Task } from '../../../entity/Task';
import {
    taskRetrievalHandler,
    CMD_TASK_TODO_USAGE,
    retrieveBacklogHandler,
    CMD_TASK_BACKLOG_USAGE
} from '../taskRetrievalHandler';
import { MessageOrigin, MessageEventType } from '../../../slack/utils';
import { ListType } from '../../../entity/List';
import { emptyList, renderTasks, listNotFound } from '../../chatResponse';

describe('taskRetrievalHandler', () => {
    const baseEvent: MessageEventType = {
        type: 'message',
        channel: 'C2147483705',
        user: 'U2147483697',
        text: 'Hello world',
        ts: '1355517523.000005',
        origin: MessageOrigin.channels
    };

    it('exists', async () => {
        expect(taskRetrievalHandler).toBeDefined();
    });

    it('processes /task_todo', async () => {
        const listEntity = require('../../../entity/List');
        const mockFindOne = jest.fn();
        listEntity.List.findOne = mockFindOne;

        await taskRetrievalHandler({ ...baseEvent, text: '/task_todo' });
        expect(mockFindOne).toHaveBeenCalled();
    });

    it('does not process /task_todo', async () => {
        const listEntity = require('../../../entity/List');
        const mockFindOne = jest.fn();
        listEntity.List.findOne = mockFindOne;

        await taskRetrievalHandler({ ...baseEvent, text: '/task_done' });
        expect(mockFindOne).not.toHaveBeenCalled();
    });

    it('processes /task_todo with tasks', async () => {
        const listEntity = require('../../../entity/List');
        const mockFindOne = jest.fn();
        listEntity.List.findOne = mockFindOne;
        const mockTask = new Task();
        mockTask.id = 'abcd';
        mockTask.title = 'task1';
        mockTask.point = 2;

        mockFindOne.mockReturnValue({
            name: ListType.Todo,
            tasks: [mockTask]
        });

        const slack = require('../../../slack');
        const sendMessageToChannelMock = jest.fn();
        slack.sendMessageToChannel = sendMessageToChannelMock;

        const res = await taskRetrievalHandler({
            ...baseEvent,
            text: '/task_todo'
        });
        expect(sendMessageToChannelMock).toHaveBeenCalledWith(
            baseEvent.channel,
            renderTasks([mockTask], ListType.Todo)
        );
    });

    it('processes /task_todo without tasks', async () => {
        const listEntity = require('../../../entity/List');
        const mockFindOne = jest.fn();
        listEntity.List.findOne = mockFindOne;
        mockFindOne.mockReturnValue({
            name: ListType.Todo,
            tasks: []
        });

        const slack = require('../../../slack');
        const sendMessageToChannelMock = jest.fn();
        slack.sendMessageToChannel = sendMessageToChannelMock;

        const res = await taskRetrievalHandler({
            ...baseEvent,
            text: '/task_todo'
        });
        expect(sendMessageToChannelMock).toHaveBeenCalledWith(
            baseEvent.channel,
            emptyList(ListType.Todo)
        );
    });

    it('rejects badly formatted /task_todo', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/task_todo asd'
        };
        const message = CMD_TASK_TODO_USAGE;

        await taskRetrievalHandler(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
    });

    describe('/task_backlog', () => {
        it('exists', async () => {
            expect(retrieveBacklogHandler).toBeDefined();
        });

        it('ignores messages not starting with /task_backlog', async () => {
            const listEntity = require('../../../entity/List');
            const mockFindOne = jest.fn().mockReturnValue(undefined);
            listEntity.List.findOne = mockFindOne;

            await retrieveBacklogHandler({ ...baseEvent, text: '/task_done' });
            expect(mockFindOne).not.toHaveBeenCalled();
        });

        it('rejects badly formatted /task_backlog', async () => {
            const slack = require('../../../slack/index');
            const mockChannel = jest.fn().mockReturnValue(true);
            slack.sendMessageToChannel = mockChannel;

            const event = {
                ...baseEvent,
                text: '/task_backlog asd'
            };
            const message = CMD_TASK_BACKLOG_USAGE;

            await retrieveBacklogHandler(event);
            expect(mockChannel).toBeCalledWith(event.channel, message);
        });

        it('processes /task_backlog with tasks', async () => {
            const listEntity = require('../../../entity/List');
            const mockFindOne = jest.fn();
            listEntity.List.findOne = mockFindOne;
            const mockTask = new Task();
            mockTask.id = 'abcd';
            mockTask.title = 'task1';
            mockTask.point = 2;

            mockFindOne.mockReturnValue({
                name: ListType.Backlog,
                tasks: [mockTask]
            });

            const slack = require('../../../slack');
            const sendMessageToChannelMock = jest.fn();
            slack.sendMessageToChannel = sendMessageToChannelMock;

            const res = await retrieveBacklogHandler({
                ...baseEvent,
                text: '/task_backlog'
            });
            expect(sendMessageToChannelMock).toHaveBeenCalledWith(
                baseEvent.channel,
                renderTasks([mockTask], ListType.Backlog)
            );
        });

        it('processes /task_backlog without tasks', async () => {
            const listEntity = require('../../../entity/List');
            const mockFindOne = jest.fn();
            listEntity.List.findOne = mockFindOne;
            mockFindOne.mockReturnValue({
                name: ListType.Backlog,
                tasks: []
            });

            const slack = require('../../../slack');
            const sendMessageToChannelMock = jest.fn();
            slack.sendMessageToChannel = sendMessageToChannelMock;

            const res = await retrieveBacklogHandler({
                ...baseEvent,
                text: '/task_backlog'
            });
            expect(sendMessageToChannelMock).toHaveBeenCalledWith(
                baseEvent.channel,
                emptyList(ListType.Backlog)
            );
        });

        it('rejects if backlog list not found', async () => {
            const slack = require('../../../slack');
            const sendMessageToChannelMock = jest.fn();
            slack.sendMessageToChannel = sendMessageToChannelMock;

            const listEntity = require('../../../entity/List');
            const mockFindOne = jest.fn().mockReturnValue(undefined);
            listEntity.List.findOne = mockFindOne;

            const res = await retrieveBacklogHandler({
                ...baseEvent,
                text: '/task_backlog'
            });

            expect(sendMessageToChannelMock).toHaveBeenCalledWith(
                baseEvent.channel,
                listNotFound(ListType.Backlog)
            );
        });
    });
});
