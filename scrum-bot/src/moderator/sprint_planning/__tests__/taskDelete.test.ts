import { MessageEventType, MessageOrigin } from '../../../slack/utils';
import { sprintHandler } from '../sprintHandler';
import { taskDelete } from '../taskDelete';

const baseEvent: MessageEventType = {
    type: 'message',
    channel: 'C2147483705',
    user: 'U2147483697',
    text: 'Hello world',
    ts: '1355517523.000005',
    origin: MessageOrigin.channels
};

describe('handler', () => {
    it('is defined', () => {
        expect(taskDelete).toBeDefined();
    });

    it('ignores unknown messages', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: 'asd'
        };
        await taskDelete(event);
        expect(mockChannel).toHaveBeenCalledTimes(0);
    });
});

describe('/task_delete', () => {
    it('rejects badly formatted message with instruction', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/task_delete '
        };
        const message = 'Usage: /task_delete [task_title]';

        await taskDelete(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
    });

    it('rejects non-existing task', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/task_delete Task 2'
        };

        const message = 'Maaf, tidak ada task Task 2';

        const taskModule = require('../../../entity/Task');
        const mockFindOne = jest.fn().mockReturnValue(undefined);
        taskModule.Task.findOne = mockFindOne;

        await taskDelete(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
        expect(mockFindOne).toBeCalledWith({ title: 'Task 2' });
    });

    it('deletes a task', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/task_delete Task 1'
        };

        const message = 'Oke, task Task 1 telah dihapus!';

        const taskModule = require('../../../entity/Task');
        const mockRemove = jest.fn();
        const mockTask = { remove: mockRemove };

        const mockFindOne = jest.fn().mockReturnValue(mockTask);
        taskModule.Task.findOne = mockFindOne;

        await taskDelete(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
        expect(mockFindOne).toBeCalledWith({ title: 'Task 1' });
        expect(mockRemove).toBeCalled();
    });
});
