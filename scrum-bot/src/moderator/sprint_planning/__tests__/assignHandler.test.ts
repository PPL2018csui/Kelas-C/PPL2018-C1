import { assignHandler } from '../assignHandler';
import { MessageEventType, MessageOrigin } from '../../../slack/utils';
import { createConnection, getRepository, getConnection } from 'typeorm';
import { List, ListType } from '../../../entity/List';
import { Task } from '../../../entity/Task';

beforeAll(async () => {
    const connection = await createConnection();
    await connection.synchronize(true);
});

afterAll(async () => {
    await getConnection().close();
});

describe('assignHandler', () => {
    const baseEvent: MessageEventType = {
        type: 'message',
        channel: 'C2147483705',
        user: 'U2147483697',
        text: 'Hello world',
        ts: '1355517523.000005',
        origin: MessageOrigin.channels,
    };

    it('is defined', () => {
        expect(assignHandler).toBeDefined();
    });

    it('test null input', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn();
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/task_assign'
        };

        await assignHandler(event);
        expect(mockChannel).toBeCalledWith(event.channel, 'Usage: /task_assign [username] [task]');
    });

    it('test undefined list', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn();
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/task_assign <@nurhaya|user1> testing'
        };

        const repositoryList = getRepository(List);
        const listTodo = new List();
        listTodo.name = ListType.Todo;
        const savedListTodo = await repositoryList.save(listTodo);

        const repositoryTask = getRepository(Task);
        const task = new Task();
        task.title = 'testing';
        task.list = listTodo;
        const savedTask = await repositoryTask.save(task);

        await assignHandler(event);
        expect(mockChannel).toBeCalledWith(event.channel, 'List doing tidak ditemukan');
    });

    it('test with list and task', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn();
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/task_assign <@nurhaya|user1> testing'
        };

        const repositoryList = getRepository(List);
        const listDoing = new List();
        listDoing.name = ListType.Doing;
        const savedListDone = await repositoryList.save(listDoing);

        await assignHandler(event);
        expect(mockChannel).toBeCalledWith(event.channel, 'Assignee sudah tersimpan dan status telah diubah');
    });

    it('test undefined task', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn();
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/task_assign <@nurhaya|user1> coba coba'
        };

        await assignHandler(event);
        expect(mockChannel).toBeCalledWith(event.channel, 'Task tidak ditemukan');
    });

    it('test wrong command', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn();
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/task_coba'
        };

        await assignHandler(event);
        expect(mockChannel).toBeCalledWith(event.channel, 'Usage: /task_assign [username] [task]');
    });
});
