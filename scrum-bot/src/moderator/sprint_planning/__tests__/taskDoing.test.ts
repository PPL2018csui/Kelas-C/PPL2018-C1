import { createConnection, getConnection } from 'typeorm';
import { List, ListType } from '../../../entity/List';
import { MessageEventType, MessageOrigin } from '../../../slack/utils';
import { Task } from '../../../entity/Task';
import { taskDoing } from '../taskDoing';
import { emptyList, renderTasks, listNotFound } from '../../chatResponse';

beforeAll(async () => {
    const connection = await createConnection();
});

beforeEach(async () => {
    await getConnection().synchronize(true);
});

afterAll(async () => {
    await getConnection().close();
});

describe('taskDoing', () => {
    const baseEvent: MessageEventType = {
        type: 'message',
        channel: 'C2147483705',
        user: 'U2147483697',
        text: 'Hello world',
        ts: '1355517523.000005',
        origin: MessageOrigin.channels
    };

    it('is defined', () => {
        expect(taskDoing).toBeDefined();
    });

    it(
        'retrieve all task_doing',
        async () => {
            const list = new List();
            list.name = ListType.Doing;
            await list.save();

            let tasks = [];
            for (let i = 0; i < 3; i++) {
                const task = new Task();
                task.list = list;
                task.title = 'Task_' + i;
                task.assignee = 'User_' + i;
                task.point = 1;
                tasks.push(await task.save());
            }

            const slack = require('../../../slack/index');
            const mockChannel = jest.fn().mockReturnValue(true);
            slack.sendMessageToChannel = mockChannel;

            const event = {
                ...baseEvent,
                text: '/task_doing'
            };

            await taskDoing(event);
            expect(mockChannel).toBeCalledWith(
                event.channel,
                renderTasks(tasks.reverse(), ListType.Doing)
            );
        },
        10000
    );

    it('There is no list in database', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(false);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/task_doing'
        };
        const message = listNotFound(ListType.Doing);

        await taskDoing(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
    });

    it('There is no task in list table', async () => {
        const list = new List();
        list.name = ListType.Doing;
        await list.save();

        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(false);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/task_doing'
        };
        const message = emptyList(ListType.Doing);

        await taskDoing(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
    });
});
