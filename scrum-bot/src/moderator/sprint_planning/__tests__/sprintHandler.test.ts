import { MessageEventType, MessageOrigin } from '../../../slack/utils';
import {
    sprintHandler,
    CMD_SPRINT_CREATE_USAGE,
    CMD_SPRINT_START_USAGE
} from '../sprintHandler';
import {
    sprintCreated,
    sprintNotFound,
    sprintStarted
} from '../../chatResponse';

const baseEvent: MessageEventType = {
    type: 'message',
    channel: 'C2147483705',
    user: 'U2147483697',
    text: 'Hello world',
    ts: '1355517523.000005',
    origin: MessageOrigin.channels
};

describe('handler', () => {
    it('is defined', () => {
        expect(sprintHandler).toBeDefined();
    });

    it('ignores unknown messages', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: 'asd'
        };
        await sprintHandler(event);
        expect(mockChannel).toHaveBeenCalledTimes(0);
    });
});

describe('/sprint_create', () => {
    it('rejects badly formatted message with instruction', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/sprint_create '
        };
        const message = CMD_SPRINT_CREATE_USAGE;

        await sprintHandler(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
    });

    it('creates a sprint', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/sprint_create Sprint 1'
        };

        const message = sprintCreated('Sprint 1');

        const sprintModule = require('../../../entity/Sprint');
        const mockSave = jest.fn();
        const mockConstructor = jest.fn().mockImplementation(() => ({
            save: mockSave
        }));
        sprintModule.Sprint = mockConstructor;

        await sprintHandler(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
        expect(mockConstructor).toBeCalled();
        expect(mockSave).toBeCalled();
    });
});

describe('/sprint_start', () => {
    it('rejects badly formatted message with instruction', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/sprint_start '
        };
        const message = CMD_SPRINT_START_USAGE;

        await sprintHandler(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
    });

    it('rejects non-existing sprint', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/sprint_start Sprint 2'
        };

        const message = sprintNotFound('Sprint 2');

        const sprintModule = require('../../../entity/Sprint');
        const mockFindOne = jest.fn().mockReturnValue(undefined);
        sprintModule.Sprint.findOne = mockFindOne;

        await sprintHandler(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
        expect(mockFindOne).toBeCalledWith({ name: 'Sprint 2' });
    });

    it('marks the sprint as active', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/sprint_start Sprint 1'
        };

        const message = sprintStarted('Sprint 1');

        const sprintModule = require('../../../entity/Sprint');
        const mockSave = jest.fn();
        const mockSprint = { save: mockSave };
        const mockActiveSetter = jest.fn();
        Object.defineProperty(mockSprint, 'active', {
            set: mockActiveSetter
        });
        const mockFindOne = jest.fn().mockReturnValue(mockSprint);
        sprintModule.Sprint.findOne = mockFindOne;

        await sprintHandler(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
        expect(mockFindOne).toBeCalledWith({ name: 'Sprint 1' });
        expect(mockActiveSetter).toBeCalledWith(true);
        expect(mockSave).toBeCalled();
    });
});
