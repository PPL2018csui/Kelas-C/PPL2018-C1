import { List, ListType } from '../../entity/List';
import { MessageEventType } from '../../slack/utils';
import { sendMessageToChannel } from '../../slack';
import { listNotFound, emptyList, renderTasks } from '../chatResponse';

export async function taskDoing(event: MessageEventType): Promise<void> {
    let results = await List.findOne({
        where: { name: ListType.Doing },
        relations: ['tasks']
    });
    if (!results) {
        sendMessageToChannel(event.channel, listNotFound(ListType.Doing));
        return;
    }

    if (results.tasks.length === 0) {
        sendMessageToChannel(event.channel, emptyList(ListType.Doing));
        return;
    }

    const tasks = results.tasks;
    sendMessageToChannel(event.channel, renderTasks(tasks, ListType.Doing));
}
