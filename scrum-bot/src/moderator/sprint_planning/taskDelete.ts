import { MessageEventType } from '../../slack/utils';
import { sendMessageToChannel } from '../../slack';
import { Task } from '../../entity/Task';

export const CMD_TASK_DELETE = '/task_delete';

export async function taskDelete(event: MessageEventType): Promise<void> {
    if (event.text.startsWith(CMD_TASK_DELETE)) {
        if (event.text.length < CMD_TASK_DELETE.length + 2) {
            sendMessageToChannel(
                event.channel,
                `Usage: ${CMD_TASK_DELETE} [task_title]`
            );
            return;
        }

        const taskTitle = event.text.slice(CMD_TASK_DELETE.length + 1);

        const task = await Task.findOne({ title: taskTitle });

        if (task === undefined) {
            sendMessageToChannel(
                event.channel,
                `Maaf, tidak ada task ${taskTitle}`
            );
            return;
        }

        await task.remove();
        sendMessageToChannel(
            event.channel,
            `Oke, task ${taskTitle} telah dihapus!`
        );
    }
}
