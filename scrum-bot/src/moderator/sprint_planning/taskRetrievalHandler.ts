import { MessageEventType } from '../../slack/utils';
import { sendMessageToChannel } from '../../slack';
import { List, ListType } from '../../entity/List';
import { renderTasks, emptyList, listNotFound } from '../chatResponse';

export const CMD_TASK_TODO = '/task_todo';
export const CMD_TASK_TODO_USAGE = `Usage: ${CMD_TASK_TODO}`;
export const CMD_TASK_BACKLOG = '/task_backlog';
export const CMD_TASK_BACKLOG_USAGE = `Usage: ${CMD_TASK_BACKLOG}`;

export async function taskRetrievalHandler(
    event: MessageEventType
): Promise<void> {
    if (event.text.startsWith(CMD_TASK_TODO)) {
        if (/^\/task_todo\s*$/.test(event.text)) {
            let res = '';
            const todoList = await List.findOne({
                where: { name: ListType.Todo },
                relations: ['tasks']
            });

            if (todoList) {
                if (!!todoList.tasks.length) {
                    res = renderTasks(todoList.tasks, ListType.Todo);
                } else {
                    res = emptyList(ListType.Todo);
                }
            }

            sendMessageToChannel(event.channel, res);
        } else {
            sendMessageToChannel(event.channel, CMD_TASK_TODO_USAGE);
        }
    }
}

export async function retrieveBacklogHandler(
    event: MessageEventType
): Promise<void> {
    if (event.text.startsWith(CMD_TASK_BACKLOG)) {
        if (/^\/task_backlog\s*$/.test(event.text)) {
            let res = '';
            const backlog = await List.findOne({
                where: { name: ListType.Backlog },
                relations: ['tasks']
            });

            if (backlog) {
                if (!!backlog.tasks.length) {
                    res = renderTasks(backlog.tasks, ListType.Backlog);
                } else {
                    res = emptyList(ListType.Backlog);
                }
            } else {
                res = listNotFound(ListType.Backlog);
            }

            sendMessageToChannel(event.channel, res);
        } else {
            sendMessageToChannel(event.channel, CMD_TASK_BACKLOG_USAGE);
        }
    }
}
