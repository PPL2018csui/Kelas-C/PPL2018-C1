import { MessageEventType } from '../../slack/utils';
import { sendMessageToChannel } from '../../slack';
import { Sprint } from '../../entity/Sprint';
import { Task } from '../../entity/Task';
import {
    sprintNotFound,
    taskNotFound,
    taskAssignedToSprint,
    listNotFound
} from '../chatResponse';
import { List, ListType } from '../../entity/List';

export const CMD_SPRINT_TASK_PLAN = '/sprint_task_plan';
export const CMD_SPRINT_TASK_PLAN_USAGE = `Usage: ${CMD_SPRINT_TASK_PLAN} [sprint_name] | [task_name]`;

export async function sprintTaskHandler(
    event: MessageEventType
): Promise<void> {
    if (event.text.startsWith(CMD_SPRINT_TASK_PLAN)) {
        const regex = new RegExp(
            `^${CMD_SPRINT_TASK_PLAN} ([^\\|]+) \\| (.+)$`
        );
        const matches = regex.exec(event.text);

        if (matches === null || matches.length !== 3) {
            sendMessageToChannel(event.channel, CMD_SPRINT_TASK_PLAN_USAGE);
            return;
        }

        const sprintName = matches[1];
        const taskTitle = matches[2];

        const sprint = await Sprint.findOne({ name: sprintName });
        if (sprint === undefined) {
            sendMessageToChannel(event.channel, sprintNotFound(sprintName));
            return;
        }

        const task = await Task.findOne({ title: taskTitle });
        if (task === undefined) {
            sendMessageToChannel(event.channel, taskNotFound(taskTitle));
            return;
        }

        const listTodo = await List.findOne({ name: ListType.Todo });
        if (listTodo === undefined) {
            sendMessageToChannel(event.channel, listNotFound(ListType.Todo));
            return;
        }

        task.sprint = sprint;
        task.list = listTodo;
        await task.save();
        sendMessageToChannel(
            event.channel,
            taskAssignedToSprint(taskTitle, sprintName)
        );
    }
}
