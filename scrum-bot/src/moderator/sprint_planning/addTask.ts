import { Task } from '../../entity/Task';
import { List, ListType } from '../../entity/List';
import { MessageEventType } from '../../slack/utils';
import { sendMessageToChannel } from '../../slack';
import { listNotFound, taskCreated, taskNotFound, taskEstimated } from '../chatResponse';

export const CMD_TASK_ADD_USAGE = 'Usage: /task_add [task_name]';
export const CMD_TASK_ESTIMATE_USAGE = 'Usage: /task_estimate [task_name] [point]';

export async function addTask(event: MessageEventType): Promise<void> {
    const regexToken = /^\/task_add (\w.*)/.exec(event.text);

    if (regexToken == null) {
        sendMessageToChannel(event.channel, CMD_TASK_ADD_USAGE);
        return;
    }

    const task = new Task();
    task.title = regexToken[1];

    const list = await List.findOne({ name: ListType.Backlog });
    if (!list) {
        sendMessageToChannel(event.channel, listNotFound(ListType.Backlog));
        return;
    }

    task.list = list;
    await task.save();

    sendMessageToChannel(event.channel, taskCreated(task.title));
}

export async function estimateTask(event: MessageEventType): Promise<void> {
    const regexToken = /^\/task_estimate (\w.*) (\d+)/.exec(event.text);

    if (regexToken == null) {
        sendMessageToChannel(event.channel, CMD_TASK_ESTIMATE_USAGE);
        return;
    }

    const title = regexToken[1];
    const point = parseInt(regexToken[2]);

    const task = await Task.findOne({ title });

    if (!task) {
        sendMessageToChannel(event.channel, taskNotFound(title));
        return;
    }

    task.point = point;
    await task.save();

    sendMessageToChannel(event.channel, taskEstimated(task.title, point));
}
