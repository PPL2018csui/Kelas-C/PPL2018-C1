import { MessageEventType } from '../../slack/utils';
import { sendMessageToChannel } from '../../slack';
import { Sprint } from '../../entity/Sprint';
import { sprintCreated, sprintNotFound, sprintStarted } from '../chatResponse';

export const CMD_SPRINT_CREATE = '/sprint_create';
export const CMD_SPRINT_CREATE_USAGE = `Usage: ${CMD_SPRINT_CREATE} [sprint_name]`;
export const CMD_SPRINT_START = '/sprint_start';
export const CMD_SPRINT_START_USAGE = `Usage: ${CMD_SPRINT_START} [sprint_name]`;

export async function sprintHandler(event: MessageEventType): Promise<void> {
    if (event.text.startsWith(CMD_SPRINT_CREATE)) {
        if (event.text.length < CMD_SPRINT_CREATE.length + 2) {
            sendMessageToChannel(event.channel, CMD_SPRINT_CREATE_USAGE);
            return;
        }

        const sprintName = event.text.slice(CMD_SPRINT_CREATE.length + 1);

        const sprint = new Sprint();
        sprint.name = sprintName;
        await sprint.save();
        sendMessageToChannel(event.channel, sprintCreated(sprintName));
    } else if (event.text.startsWith(CMD_SPRINT_START)) {
        if (event.text.length < CMD_SPRINT_START.length + 2) {
            sendMessageToChannel(event.channel, CMD_SPRINT_START_USAGE);
            return;
        }

        const sprintName = event.text.slice(CMD_SPRINT_START.length + 1);

        const sprint = await Sprint.findOne({ name: sprintName });

        if (sprint === undefined) {
            sendMessageToChannel(event.channel, sprintNotFound(sprintName));
            return;
        }

        sprint.active = true;
        await sprint.save();
        sendMessageToChannel(event.channel, sprintStarted(sprintName));
    }
}
