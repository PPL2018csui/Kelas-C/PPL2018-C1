import { MessageEventType } from '../../slack/utils';
import { sendMessageToChannel } from '../../slack';
import { Sprint } from '../../entity/Sprint';


export async function makeSummary(event: MessageEventType): Promise<void> {
    let res = '';
    let results = await Sprint.findOne({ order: { createdAt: 'DESC' }, where: { active: false }, relations: ['tasks'] });
    if (!results) {
        res = 'Mulai sprint planning terlebih dahulu.';
        sendMessageToChannel(event.channel, res);
        return;
    }

    if ( results.tasks.length === 0) {
        res = 'Tidak ada task yang sedang dikerjakan.';
        sendMessageToChannel(event.channel, res);
        return;
    }

    const tasks = results.tasks;
    let points = 0;
    res += `Berikut merupakan summary dari sprint planning untuk sprint ${results.name} yang telah dilakukan \n`;
    tasks.forEach((task) => {
        if (!!task.assignee) {
            res += `${task.title} (<@${task.assignee}>) - Bobot: ${task.point}\n`;
        } else {
            res += `${task.title} - Bobot: ${task.point}\n`;
        }
        if (task.point) {
            points += task.point;
        }
    });
    res += 'Total bobot yang diambil adalah ' + points + '\n';
    res += 'Terima kasih atas partisipasinya!';
    sendMessageToChannel(event.channel, res);
    return;
}