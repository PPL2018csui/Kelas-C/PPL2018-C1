import { MessageEventType } from '../../slack/utils';
import { sendMessageToChannel } from '../../slack';
import { Task } from '../../entity/Task';
import { List, ListType } from '../../entity/List';

export async function assignHandler(event: MessageEventType): Promise<void> {
    if (event.text.startsWith('/task_assign')) {
        const userName = /<@([^|>]+)\|([^>]+)>/g.exec(event.text);
        const taskName = /\> (.*)/.exec(event.text);
        if (userName === null || userName.length !== 3 || taskName === null) {
            sendMessageToChannel(event.channel, 'Usage: /task_assign [username] [task]');
        } else {
            let assignee = userName[1];
            let currTask = taskName[1];
            let task = await Task.findOne({ where: { title: currTask} , relations: ['list'] });
            let listDoing = await List.findOne({ where: { name: ListType.Doing }});
            if (task === undefined) {
                sendMessageToChannel(event.channel, 'Task tidak ditemukan');
            } else if (listDoing === undefined) {
                sendMessageToChannel(event.channel, 'List doing tidak ditemukan');
            } else {
                task.assignee = assignee;
                task.list = listDoing;
                await task.save();
                sendMessageToChannel(event.channel, 'Assignee sudah tersimpan dan status telah diubah');
            }
        }
    } else {
        sendMessageToChannel(event.channel, 'Usage: /task_assign [username] [task]');
    }
    return;
}
