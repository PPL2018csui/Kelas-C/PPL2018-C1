import {
    emptyList,
    listNotFound,
    renderTasks,
    reviewDone,
    reviewed,
    sprintCreated,
    sprintStarted,
    sprintNotFound,
    taskCreated,
    taskNotFound,
    taskAssignedToSprint,
    taskEstimated
} from '../chatResponse';
import { ListType } from '../../entity/List';
import { Task } from '../../entity/Task';
import { render } from 'nunjucks';

describe('emptyList', () => {
    it('is defined', () => {
        expect(emptyList).toBeDefined();
    });

    it('returns message correctly', () => {
        const list = ListType.Todo;
        expect(emptyList(list)).toBe(
            `Maaf, tidak ada task dalam list '${ListType[list]}'`
        );
    });
});

describe('listNotFound', () => {
    it('is defined', () => {
        expect(listNotFound).toBeDefined();
    });

    it('returns message correctly', () => {
        const list = ListType.Todo;
        expect(listNotFound(list)).toBe(
            `Maaf, tidak ada list '${ListType[list]}'`
        );
    });
});

describe('renderTask', () => {
    it('is defined', () => {
        expect(renderTasks).toBeDefined();
    });

    it('returns message correctly without assignee', () => {
        const task = new Task();
        task.title = 'abcd';
        const list = ListType.Todo;
        expect(renderTasks([task], list)).toBe(`Todo:\n1) abcd\n`);
    });

    it('returns message correctly with assignee', () => {
        const task = new Task();
        task.title = 'abcd';
        task.assignee = 'efgh';
        const list = ListType.Doing;
        expect(renderTasks([task], list)).toBe(`Doing:\n1) abcd - <@efgh>\n`);
    });
});

describe('reviewDone', () => {
    it('is defined', () => {
        expect(reviewDone).toBeDefined();
    });

    it('returns message correctly', () => {
        expect(reviewDone()).toBe('Oke! Terima kasih atas partisipasinya!');
    });
});

describe('reviewed', () => {
    it('is defined', () => {
        expect(reviewed).toBeDefined();
    });

    it('returns message correctly', () => {
        expect(reviewed()).toBe('Oke, pesanmu telah disimpan! Ada lagi?');
    });
});

describe('sprintCreated', () => {
    it('is defined', () => {
        expect(sprintCreated).toBeDefined();
    });

    it('returns message correctly', () => {
        const sprintName = 'Sprint 1';
        expect(sprintCreated(sprintName)).toBe(
            `Oke, sprint '${sprintName}' telah dibuat! Silakan assign task-task dan assigneenya ke dalam sprint ini!`
        );
    });
});

describe('sprintStarted', () => {
    it('is defined', () => {
        expect(sprintStarted).toBeDefined();
    });

    it('returns message correctly', () => {
        const sprintName = 'Sprint 1';
        expect(sprintStarted(sprintName)).toBe(
            `Oke, sprint '${sprintName}' telah dimulai!`
        );
    });
});

describe('sprintNotFound', () => {
    it('is defined', () => {
        expect(sprintNotFound).toBeDefined();
    });

    it('returns message correctly', () => {
        const sprintName = 'Sprint 1';
        expect(sprintNotFound(sprintName)).toBe(
            `Maaf, tidak ada sprint '${sprintName}'`
        );
    });
});

describe('taskCreated', () => {
    it('is defined', () => {
        expect(taskCreated).toBeDefined();
    });

    it('returns message correctly', () => {
        const taskTitle = 'Task 1';
        expect(taskCreated(taskTitle)).toBe(
            `Oke, task '${taskTitle}' telah dibuat!`
        );
    });
});

describe('taskCreated', () => {
    it('is defined', () => {
        expect(taskEstimated).toBeDefined();
    });

    it('returns message correctly', () => {
        const taskTitle = 'Task 1';
        expect(taskEstimated(taskTitle, 1)).toBe(
            `Oke, estimate task '${taskTitle}' telah diubah menjadi 1!`
        );
    });
});

describe('taskNotFound', () => {
    it('is defined', () => {
        expect(taskNotFound).toBeDefined();
    });

    it('returns message correctly', () => {
        const taskTitle = 'Task 1';
        expect(taskNotFound(taskTitle)).toBe(
            `Maaf, tidak ada task '${taskTitle}'`
        );
    });
});

describe('taskAssignedToSprint', () => {
    it('is defined', () => {
        expect(taskAssignedToSprint).toBeDefined();
    });

    it('returns message correctly', () => {
        const taskTitle = 'Task 1';
        const sprintName = 'Sprint 1';
        expect(taskAssignedToSprint(taskTitle, sprintName)).toBe(
            `Oke, '${taskTitle}' telah dimasukkan ke '${sprintName}'!`
        );
    });
});
