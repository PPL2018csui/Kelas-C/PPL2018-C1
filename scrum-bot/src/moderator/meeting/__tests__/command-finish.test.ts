import {commandFinish} from '../command-finish';
import {MessageEventType, MessageOrigin} from '../../../slack/utils';
import {createConnection, getRepository, getConnection} from 'typeorm';
import {List, ListType} from '../../../entity/List';
import {Task} from '../../../entity/Task';

beforeAll(async () => {
    const connection = await createConnection();
    await connection.synchronize(true);
});

afterAll(async () => {
    await getConnection().close();
});

describe('commandFinish', () => {
    const baseEvent: MessageEventType = {
        type: 'message',
        channel: 'C2147483705',
        user: 'U2147483697',
        text: 'Hello world',
        ts: '1355517523.000005',
        origin: MessageOrigin.channels,
    };

    it('is defined', () => {
        expect(commandFinish).toBeDefined();
    });

    it('test undefined list', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn();
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/task_finish testing'
        };

        const repositoryList = getRepository(List);
        const listDoing = new List();
        listDoing.name = ListType.Doing;
        const savedListDoing = await repositoryList.save(listDoing);

        const repositoryTask = getRepository(Task);
        const task = new Task();
        task.title = 'testing';
        task.list = listDoing;
        const savedTask = await repositoryTask.save(task);

        await commandFinish(event);
        expect(mockChannel).toBeCalledWith(event.channel, 'List done tidak ada');
    });

    it('change task with doing status to done', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn();
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/task_finish testing'
        };

        const repositoryList = getRepository(List);
        const listDone = new List();
        listDone.name = ListType.Done;
        const savedListDone = await repositoryList.save(listDone);

        await commandFinish(event);
        expect(mockChannel).toBeCalledWith(event.channel, 'Status task telah diubah');
    });

    it('test undefined task', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn();
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/task_finish coba'
        };

        await commandFinish(event);
        expect(mockChannel).toBeCalledWith(event.channel, 'Task yang dimasukkan tidak ada');
    });

    it('test wrong command', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn();
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/task_done'
        };

        await commandFinish(event);
        expect(mockChannel).toBeCalledWith(event.channel, 'Usage: /task_finish [nama task]');
    });
});
