import { commandDone, CMD_TASK_DONE_USAGE } from '../command-done';
import { MessageEventType, MessageOrigin } from '../../../slack/utils';
import { createConnection, getRepository, getConnection } from 'typeorm';
import { List, ListType } from '../../../entity/List';
import { Task } from '../../../entity/Task';
import { emptyList, renderTasks, listNotFound } from '../../chatResponse';

beforeAll(async () => {
    const connection = await createConnection();
    await connection.synchronize(true);
});

afterAll(async () => {
    await getConnection().close();
});

describe('commandDone', () => {
    const baseEvent: MessageEventType = {
        type: 'message',
        channel: 'C2147483705',
        user: 'U2147483697',
        text: 'Hello world',
        ts: '1355517523.000005',
        origin: MessageOrigin.channels
    };

    it('is defined', () => {
        expect(commandDone).toBeDefined();
    });

    it('get task with done status (no data)', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn();
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/task_done'
        };

        await commandDone(event);
        expect(mockChannel).toBeCalledWith(
            event.channel,
            listNotFound(ListType.Done)
        );
    });

    it('get task with done status (no task)', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn();
        slack.sendMessageToChannel = mockChannel;

        const repository = getRepository(List);
        const list = new List();
        list.name = ListType.Done;
        const savedList = await repository.save(list);

        const event = {
            ...baseEvent,
            text: '/task_done'
        };

        await commandDone(event);
        expect(mockChannel).toBeCalledWith(
            event.channel,
            emptyList(ListType.Done)
        );
    });

    it('get task with done status (with data)', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn();
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/task_done'
        };

        const repository = getRepository(List);
        const list = new List();
        list.name = ListType.Done;
        const savedList = await repository.save(list);

        const temp = [];
        const repositoryTask = getRepository(Task);
        for (let i = 0; i < 3; i++) {
            const task = new Task();
            task.title = 'testing';
            task.list = list;
            const savedTask = await repositoryTask.save(task);
            temp.push(savedTask);
        }

        await commandDone(event);

        expect(mockChannel).toBeCalledWith(
            event.channel,
            renderTasks(temp, ListType.Done)
        );
    });

    it('test wrong trailing command', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn();
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/task_done hello'
        };

        await commandDone(event);
        expect(mockChannel).toBeCalledWith(event.channel, CMD_TASK_DONE_USAGE);
    });
});
