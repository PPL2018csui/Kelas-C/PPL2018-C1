import {MessageEventType} from '../../slack/utils';
import {sendMessageToChannel} from '../../slack';
import {List, ListType} from '../../entity/List';
import {Task} from '../../entity/Task';

export async function commandFinish(event: MessageEventType): Promise<void> {
    if (event.text.startsWith('/task_finish')) {
        let masukan = event.text.substring(13);
        let cariTask = await Task.findOne({ where: { title: masukan} , relations: ['list'] });
        let listTujuan = await List.findOne({ where: {name: ListType.Done}});
        if (cariTask === undefined) {
            sendMessageToChannel(event.channel, 'Task yang dimasukkan tidak ada');
        } else if (listTujuan === undefined) {
            sendMessageToChannel(event.channel, 'List done tidak ada');
        } else {
            cariTask.list = listTujuan;
            await cariTask.save();
            sendMessageToChannel(event.channel, 'Status task telah diubah');
        }
    } else {
        sendMessageToChannel(event.channel, 'Usage: /task_finish [nama task]');
    }
    return;
}
