import { MessageEventType } from '../../slack/utils';
import { sendMessageToChannel } from '../../slack';
import { List, ListType } from '../../entity/List';
import { emptyList, renderTasks, listNotFound } from '../chatResponse';

export const CMD_TASK_DONE_USAGE = 'Usage: /task_done';

export async function commandDone(event: MessageEventType): Promise<void> {
    let selesai = await List.find({
        where: { name: ListType.Done },
        relations: ['tasks']
    });

    if (/^\/task_done\s*$/.test(event.text)) {
        if (selesai.length !== 0) {
            let tasks = [];
            for (let i = 0; i < selesai.length; i++) {
                for (let j = 0; j < selesai[i].tasks.length; j++) {
                    tasks.push(selesai[i].tasks[j]);
                }
            }
            if (tasks.length > 0) {
                sendMessageToChannel(
                    event.channel,
                    renderTasks(tasks, ListType.Done)
                );
            } else {
                sendMessageToChannel(event.channel, emptyList(ListType.Done));
            }
        } else {
            sendMessageToChannel(event.channel, listNotFound(ListType.Done));
        }
    } else {
        sendMessageToChannel(event.channel, CMD_TASK_DONE_USAGE);
    }
    return;
}
