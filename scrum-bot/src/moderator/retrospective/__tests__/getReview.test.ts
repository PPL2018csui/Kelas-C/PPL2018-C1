import { getReview } from '../getReview';
import { MessageEventType, MessageOrigin } from '../../../slack/utils';
import { createConnection, getRepository, getConnection } from 'typeorm';
import { Comment } from '../../../entity/Comment';

beforeAll(async () => {
    const connection = await createConnection();
});

afterAll(async () => {
    await getConnection().close();
});

describe('getReview', () => {
    const baseEvent: MessageEventType = {
        type: 'message',
        channel: 'C2147483705',
        user: 'U2147483697',
        text: 'Hello world',
        ts: '1355517523.000005',
        origin: MessageOrigin.channels,
    };

    it('is defined', () => {
        expect(getReview).toBeDefined();
    });

    it('test wrong command', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn();
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/get_summary @harun'
        };

        await getReview(event);
        expect(mockChannel).toBeCalledWith(event.channel, 'Usage: /get_retrospective [username]');
    });

    it('test wrong input', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn();
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/get_retrospective @harun'
        };

        await getReview(event);
        expect(mockChannel).toBeCalledWith(event.channel, 'Usage: /get_retrospective [username]');
    });

    it('test with comment', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn();
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/get_retrospective <@harun|user1>'
        };

        const komentar = new Comment();
        komentar.from = 'rakha';
        komentar.to = 'harun';
        komentar.message = 'Usaha lebih keras lagi';
        await komentar.save();

        const komentar2 = new Comment();
        komentar2.from = 'rakha';
        komentar2.to = 'harun';
        komentar2.message = 'Tetap semangat';
        await komentar2.save();

        await getReview(event);
        expect(mockChannel).toBeCalledWith(event.channel, '1) Usaha lebih keras lagi\n2) Tetap semangat');
    });

    it('test no comment', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn();
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/get_retrospective <@rakha|user1>'
        };

        await getReview(event);
        expect(mockChannel).toBeCalledWith(event.channel, 'Tidak ada komentar yang tersedia');
    });
});
