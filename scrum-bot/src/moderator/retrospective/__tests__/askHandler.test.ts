import { askHandler, CMD_REVIEW_USAGE } from '../askHandler';
import { MessageEventType, MessageOrigin } from '../../../slack/utils';
import { reviewed, reviewDone } from '../../chatResponse';

describe('askHandler', () => {
    const baseEvent: MessageEventType = {
        type: 'message',
        channel: 'C2147483705',
        user: 'U2147483697',
        text: 'Hello world',
        ts: '1355517523.000005',
        origin: MessageOrigin.channels
    };

    it('is defined', () => {
        expect(askHandler).toBeDefined();
    });

    it('saves user comment for another user (/retrospective)', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const commentEntity = require('../../../entity/Comment');
        const mockCommentSave = jest.fn();
        const mockCommentConstructor = jest.fn().mockImplementation(() => ({
            save: mockCommentSave
        }));
        commentEntity.Comment = mockCommentConstructor;

        const event = {
            ...baseEvent,
            text: '/retrospective <@USER_ID|user2> test'
        };
        const message = reviewed();

        await askHandler(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
        expect(mockCommentConstructor).toBeCalled();
        expect(mockCommentSave).toBeCalled();
    });

    it('rejects badly formatted /retrospective', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: '/retrospective asd'
        };
        const message = CMD_REVIEW_USAGE;

        await askHandler(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
    });

    it('closes the session when received /retrospective_done', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const user = 'asd';
        const event = {
            ...baseEvent,
            text: '/retrospective_done'
        };
        const message = reviewDone();

        await askHandler(event);
        expect(mockChannel).toBeCalledWith(event.channel, message);
    });

    it('ignores other messages', async () => {
        const slack = require('../../../slack/index');
        const mockChannel = jest.fn().mockReturnValue(true);
        slack.sendMessageToChannel = mockChannel;

        const event = {
            ...baseEvent,
            text: 'asd'
        };
        await askHandler(event);
        expect(mockChannel).toHaveBeenCalledTimes(0);
    });
});
