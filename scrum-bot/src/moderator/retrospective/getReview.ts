import { MessageEventType } from '../../slack/utils';
import { sendMessageToChannel } from '../../slack';
import { Comment } from '../../entity/Comment';

export async function getReview(event: MessageEventType): Promise<void> {
    if (!event.text.startsWith('/get_retrospective')) {
        sendMessageToChannel(event.channel, 'Usage: /get_retrospective [username]');
        return;
    }

    const userName = /<@([^|>]+)\|([^>]+)>/g.exec(event.text);
    if (userName === null || userName.length !== 3) {
        sendMessageToChannel(event.channel, 'Usage: /get_retrospective [username]');
        return;
    }

    const id = userName[1];
    const komentar = await Comment.find({ where: { to: id }});
    let keluaran = '';
    if (komentar.length === 0) {
        keluaran = 'Tidak ada komentar yang tersedia';
    } else {
        for (let i = 0; i < komentar.length; i++) {
            if (i === 0) {
                keluaran += `${i + 1}) ${komentar[i].message}`;
            } else {
                keluaran += `\n${i + 1}) ${komentar[i].message}`;
            }
        }
    }
    sendMessageToChannel(event.channel, keluaran);
    return;
}
