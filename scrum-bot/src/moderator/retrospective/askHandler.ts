import { MessageEventType } from '../../slack/utils';
import { sendMessageToChannel } from '../../slack';
import { Comment } from '../../entity/Comment';
import { reviewDone, reviewed } from '../chatResponse';

export const CMD_REVIEW_USAGE = 'Usage: /retrospective [username] [pesan]';

export async function askHandler(event: MessageEventType): Promise<void> {
    if (event.text.startsWith('/retrospective_done')) {
        sendMessageToChannel(event.channel, reviewDone());
    } else if (event.text.startsWith('/retrospective')) {
        const tokens = /<@([^|>]+)\|([^>]+)>/g.exec(event.text);
        if (tokens === null || tokens.length !== 3) {
            sendMessageToChannel(event.channel, CMD_REVIEW_USAGE);
        } else {
            const receiver = tokens[1];
            const comment = new Comment();
            comment.from = event.user;
            comment.to = receiver;
            comment.message = event.text.slice(event.text.indexOf('> ') + 1);
            await comment.save();
            sendMessageToChannel(event.channel, reviewed());
        }
    }

    return;
}
