import { ListType } from '../entity/List';
import { Task } from '../entity/Task';

export function renderTasks(tasks: Task[], list: ListType): string {
    let res = `${ListType[list]}:\n`;

    tasks.forEach((task, idx) => {
        res += `${idx + 1}) ${task.title}`;
        if (!!task.assignee) {
            res += ` - <@${task.assignee}>`;
        }
        res += '\n';
    });

    return res;
}

export function emptyList(list: ListType): string {
    return `Maaf, tidak ada task dalam list '${ListType[list]}'`;
}

export function listNotFound(list: ListType): string {
    return `Maaf, tidak ada list '${ListType[list]}'`;
}

export function sprintNotFound(sprintName: string): string {
    return `Maaf, tidak ada sprint '${sprintName}'`;
}

export function taskNotFound(taskTitle: string): string {
    return `Maaf, tidak ada task '${taskTitle}'`;
}

export function taskCreated(taskTitle: string): string {
    return `Oke, task '${taskTitle}' telah dibuat!`;
}

export function taskEstimated(taskTitle: string, point: number): string {
    return `Oke, estimate task '${taskTitle}' telah diubah menjadi ${point}!`;
}

export function sprintCreated(sprintName: string): string {
    return `Oke, sprint '${sprintName}' telah dibuat! Silakan assign task-task dan assigneenya ke dalam sprint ini!`;
}

export function sprintStarted(sprintName: string): string {
    return `Oke, sprint '${sprintName}' telah dimulai!`;
}

export function taskAssignedToSprint(
    taskTitle: string,
    sprintName: string
): string {
    return `Oke, '${taskTitle}' telah dimasukkan ke '${sprintName}'!`;
}

export function reviewDone(): string {
    return 'Oke! Terima kasih atas partisipasinya!';
}

export function reviewed(): string {
    return 'Oke, pesanmu telah disimpan! Ada lagi?';
}
