import { MigrationInterface, QueryRunner } from 'typeorm';

export class Initial1520747095707 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(
            `CREATE TABLE "blocker" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "user" character varying NOT NULL, "text" text NOT NULL, "status" character varying NOT NULL, PRIMARY KEY("id"))`
        );
        await queryRunner.query(
            `CREATE TABLE "comment" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "from" character varying NOT NULL, "to" character varying NOT NULL, "message" text NOT NULL, PRIMARY KEY("id"))`
        );
        await queryRunner.query(
            `CREATE TABLE "project" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying NOT NULL, PRIMARY KEY("id"))`
        );
        await queryRunner.query(
            `CREATE TABLE "task" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "title" character varying NOT NULL, "assignee" character varying, "listId" uuid, PRIMARY KEY("id"))`
        );
        await queryRunner.query(
            `CREATE TABLE "list" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying NOT NULL, "projectId" uuid, PRIMARY KEY("id"))`
        );
        await queryRunner.query(
            `ALTER TABLE "task" ADD CONSTRAINT "fk_5849bbdc14f70f9cc75eb7024ff" FOREIGN KEY ("listId") REFERENCES "list"("id")`
        );
        await queryRunner.query(
            `ALTER TABLE "list" ADD CONSTRAINT "fk_6e0784bee3db57e0c50db74b764" FOREIGN KEY ("projectId") REFERENCES "project"("id")`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(
            `ALTER TABLE "list" DROP CONSTRAINT "fk_6e0784bee3db57e0c50db74b764"`
        );
        await queryRunner.query(
            `ALTER TABLE "task" DROP CONSTRAINT "fk_5849bbdc14f70f9cc75eb7024ff"`
        );
        await queryRunner.query(`DROP TABLE "list"`);
        await queryRunner.query(`DROP TABLE "task"`);
        await queryRunner.query(`DROP TABLE "project"`);
        await queryRunner.query(`DROP TABLE "comment"`);
        await queryRunner.query(`DROP TABLE "blocker"`);
    }
}
