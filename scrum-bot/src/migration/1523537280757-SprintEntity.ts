import { MigrationInterface, QueryRunner } from 'typeorm';

export class SprintEntity1523537280757 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "public"."sprint" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying NOT NULL, "active" boolean NOT NULL, "projectId" uuid, PRIMARY KEY("id"))`);
        await queryRunner.query(`ALTER TABLE "public"."task" ADD "sprintId" uuid`);
        await queryRunner.query(`ALTER TABLE "public"."task" ADD CONSTRAINT "fk_1d4867ccc3c73b2aa9b207bb767" FOREIGN KEY ("sprintId") REFERENCES "public"."sprint"("id")`);
        await queryRunner.query(`ALTER TABLE "public"."sprint" ADD CONSTRAINT "fk_b90e964cfbc9d13ce61d8b6519a" FOREIGN KEY ("projectId") REFERENCES "public"."project"("id")`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "public"."sprint" DROP CONSTRAINT "fk_b90e964cfbc9d13ce61d8b6519a"`);
        await queryRunner.query(`ALTER TABLE "public"."task" DROP CONSTRAINT "fk_1d4867ccc3c73b2aa9b207bb767"`);
        await queryRunner.query(`ALTER TABLE "public"."task" DROP "sprintId"`);
        await queryRunner.query(`DROP TABLE "sprint"`);
    }

}
