import {MigrationInterface, QueryRunner} from 'typeorm';

export class Review1524640540678 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "public"."review" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "text" character varying NOT NULL, "type" integer NOT NULL, PRIMARY KEY("id"))`);
        await queryRunner.query(`ALTER TABLE "public"."task" ALTER COLUMN "point" TYPE integer`);
        await queryRunner.query(`ALTER TABLE "public"."task" ALTER COLUMN "point" DROP DEFAULT`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`-- TODO: revert ALTER TABLE "public"."task" ALTER COLUMN "point" DROP DEFAULT`);
        await queryRunner.query(`-- TODO: revert ALTER TABLE "public"."task" ALTER COLUMN "point" TYPE integer`);
        await queryRunner.query(`DROP TABLE "review"`);
    }

}
