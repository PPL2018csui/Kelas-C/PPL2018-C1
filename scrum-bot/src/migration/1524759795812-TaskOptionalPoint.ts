import { MigrationInterface, QueryRunner } from 'typeorm';

export class TaskOptionalPoint1524759795812 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "public"."task" ALTER COLUMN "point" TYPE integer`);
        await queryRunner.query(`ALTER TABLE "public"."task" ALTER COLUMN "point" DROP DEFAULT`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`-- TODO: revert ALTER TABLE "public"."task" ALTER COLUMN "point" DROP DEFAULT`);
        await queryRunner.query(`-- TODO: revert ALTER TABLE "public"."task" ALTER COLUMN "point" TYPE integer`);
    }

}
