import { MigrationInterface, QueryRunner } from 'typeorm';
import { BlockerStatus } from '../entity/Blocker';

export class AlterBlockerStatusToEnum1520791586419
    implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(
            `
            UPDATE "public"."blocker" SET "status"='${
                BlockerStatus.Pending
            }' WHERE "status"='pending';
            UPDATE "public"."blocker" SET "status"='${
                BlockerStatus.Solved
            }' WHERE "status"='solved';
            UPDATE "public"."blocker" SET "status"='${
                BlockerStatus.Hold
            }' WHERE "status"='hold';
            `
        );
        await queryRunner.query(
            `ALTER TABLE "public"."blocker" ALTER COLUMN "status" TYPE integer USING status::integer`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(
            `ALTER TABLE "public"."blocker" ALTER COLUMN "status" TYPE character varying`
        );
        await queryRunner.query(
            `
            UPDATE "public"."blocker" SET "status"='pending' WHERE "status"='${
                BlockerStatus.Pending
            }';
            UPDATE "public"."blocker" SET "status"='solved' WHERE "status"='${
                BlockerStatus.Solved
            }';
            UPDATE "public"."blocker" SET "status"='hold' WHERE "status"='${
                BlockerStatus.Hold
            }';
            `
        );
    }
}
