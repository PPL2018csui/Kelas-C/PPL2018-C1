import { getConnection, createConnection } from 'typeorm';

test('migrations can be applied and reverted', async () => {
    const connection = await createConnection();
    await connection.query('DROP SCHEMA PUBLIC CASCADE');
    await connection.query('CREATE SCHEMA PUBLIC');
    await connection.query('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"');
    await connection.runMigrations();
    const { length } = connection.migrations;
    for (let i = 0; i < length; i++) {
        await connection.undoLastMigration();
    }
    await connection.synchronize(true);
    await connection.close();
});
