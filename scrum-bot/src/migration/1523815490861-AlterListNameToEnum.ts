import { MigrationInterface, QueryRunner } from 'typeorm';
import { ListType } from '../entity/List';

export class AlterListNameToEnum1523815490861 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(
            `
            UPDATE "public"."list" SET "name"='${
                ListType.Todo
            }' WHERE LOWER("name")='to do' OR LOWER("name")='todo';
            UPDATE "public"."list" SET "name"='${
                ListType.Doing
            }' WHERE LOWER("name")='in progress' OR LOWER("name")='doing';
            UPDATE "public"."list" SET "name"='${
                ListType.Done
            }' WHERE LOWER("name")='done';
            `
        );
        await queryRunner.query(
            `ALTER TABLE "public"."list" ALTER COLUMN "name" TYPE integer USING name::integer`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(
            `ALTER TABLE "public"."list" ALTER COLUMN "name" TYPE character varying`
        );
        await queryRunner.query(
            `
            UPDATE "public"."list" SET "name"='to do' WHERE "name"='${
                ListType.Todo
            }';
            UPDATE "public"."list" SET "name"='doing' WHERE "name"='${
                ListType.Doing
            }';
            UPDATE "public"."list" SET "name"='done' WHERE "name"='${
                ListType.Done
            }';
            `
        );
    }

}
