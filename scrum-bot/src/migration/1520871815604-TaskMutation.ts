import { MigrationInterface, QueryRunner } from 'typeorm';

export class TaskMutation1520871815604 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(
            `CREATE TABLE "public"."task_mutation" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "taskId" uuid, "beforeId" uuid, "afterId" uuid, PRIMARY KEY("id"))`
        );
        await queryRunner.query(
            `ALTER TABLE "public"."task_mutation" ADD CONSTRAINT "fk_1561bf1bc4b1e36b475b142d3af" FOREIGN KEY ("taskId") REFERENCES "public"."task"("id")`
        );
        await queryRunner.query(
            `ALTER TABLE "public"."task_mutation" ADD CONSTRAINT "fk_23b41a1a62a953062b83e6c0055" FOREIGN KEY ("beforeId") REFERENCES "public"."list"("id")`
        );
        await queryRunner.query(
            `ALTER TABLE "public"."task_mutation" ADD CONSTRAINT "fk_0d94e9e052f524ac13de8e43cab" FOREIGN KEY ("afterId") REFERENCES "public"."list"("id")`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(
            `ALTER TABLE "public"."task_mutation" DROP CONSTRAINT "fk_0d94e9e052f524ac13de8e43cab"`
        );
        await queryRunner.query(
            `ALTER TABLE "public"."task_mutation" DROP CONSTRAINT "fk_23b41a1a62a953062b83e6c0055"`
        );
        await queryRunner.query(
            `ALTER TABLE "public"."task_mutation" DROP CONSTRAINT "fk_1561bf1bc4b1e36b475b142d3af"`
        );
        await queryRunner.query(`DROP TABLE "task_mutation"`);
    }
}
