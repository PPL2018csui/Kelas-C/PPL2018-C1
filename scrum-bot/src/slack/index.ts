import axios from 'axios';

export enum MessageDeliveryStatus {
    SENT,
    FAILED
}

export async function openConversation(
    userId: string,
    token = process.env.SLACK_BOT_USER_TOKEN
): Promise<boolean> {
    return axios
        .post(
            `https://slack.com/api/conversations.open?token=${token}&users=${userId}`
        )
        .then(response => {
            if (!response.data.ok) return false;
            let conversationId = response.data.channel.id;
            return conversationId;
        });
}

export async function sendDirectMessage(
    userId: string,
    text: string,
    token = process.env.SLACK_BOT_USER_TOKEN
): Promise<MessageDeliveryStatus> {
    let channelId = openConversation(userId);
    return axios
        .post(
            `https://slack.com/api/chat.postMessage?token=${token}&channel=${channelId}&text=${text}`
        )
        .then(response => {
            if (!response.data.ok) return MessageDeliveryStatus.FAILED;
            return MessageDeliveryStatus.SENT;
        });
}

export async function sendMessageToChannel(
    channelId: string,
    text: string,
    token = process.env.SLACK_BOT_USER_TOKEN): Promise<MessageDeliveryStatus> {
    return axios
        .post(`https://slack.com/api/chat.postMessage?token=${token}&channel=${channelId}&text=${text}`)
        .then(response => {
            if (!response.data.ok) return MessageDeliveryStatus.FAILED;
            return MessageDeliveryStatus.SENT;
        });
}
