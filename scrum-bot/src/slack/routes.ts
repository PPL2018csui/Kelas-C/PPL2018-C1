import Koa from 'koa';
import Router from 'koa-router';
import { isMemberOf, channelTypes, getSlackAccessToken, ChannelType, messageHandler, MessageEventType, MessageOrigin } from './utils';

const router = new Router({
    prefix: '/slack'
});

router.post('/events', async (ctx: Koa.Context) => {
    if (ctx.request.body.token !== process.env.SLACK_VERIFICATION_TOKEN) {
        ctx.status = 401;
        return;
    }
    if (ctx.request.body.type === 'url_verification') {
        ctx.body = { challenge: ctx.request.body.challenge };
        return;
    }
    ctx.status = 200;
    if (
        ctx.request.body.type === 'event_callback' &&
        ctx.request.body.event.type === 'message'
    ) {
        const messageEvent: MessageEventType = ctx.request.body.event;
        let origin = MessageOrigin.unknown;
        if (
            await isMemberOf(
                ctx.request.body.event.channel,
                channelTypes.find(type => type.endpoint === 'channels')!
            )
        ) {
            origin = MessageOrigin.channels;
        } else if (
            await isMemberOf(
                ctx.request.body.event.channel,
                channelTypes.find(type => type.endpoint === 'im')!
            )
        ) {
            origin = MessageOrigin.ims;
        } else if (
            await isMemberOf(
                ctx.request.body.event.channel,
                channelTypes.find(type => type.endpoint === 'mpim')!
            )
        ) {
            origin = MessageOrigin.mpim;
        } else if (
            await isMemberOf(
                ctx.request.body.event.channel,
                channelTypes.find(type => type.endpoint === 'groups')!
            )
        ) {
            origin = MessageOrigin.groups;
        }

        messageEvent.origin = origin;
        messageHandler(messageEvent);
    }
});

router.post('/commands/:command', async (ctx: Koa.Context) => {
    if (ctx.request.body.token !== process.env.SLACK_VERIFICATION_TOKEN) {
        ctx.status = 401;
        return;
    }

    ctx.body = { response_type: 'in_channel' };
    ctx.status = 200;
});

router.get('/oauth', async (ctx: Koa.Context) => {
    const error = ctx.query['error'];

    if (typeof error === 'undefined') {
        const code = ctx.query['code'];
        const accessToken = await getSlackAccessToken(code);
        ctx.body = accessToken;
    } else {
        ctx.redirect('/');
    }
});

export { router };
