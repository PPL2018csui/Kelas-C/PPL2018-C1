import axios from 'axios';
import { assignHandler } from '../moderator/sprint_planning/assignHandler';
import { askHandler } from '../moderator/retrospective/askHandler';
import { redSprintReview, greenSprintReview, showSprintReview } from '../moderator/review/sprintReview';
import { commandFinish } from '../moderator/meeting/command-finish';
import { addTask } from '../moderator/sprint_planning/addTask';
import {
    taskRetrievalHandler,
    retrieveBacklogHandler
} from '../moderator/sprint_planning/taskRetrievalHandler';
import { commandDone } from '../moderator/meeting/command-done';
import { taskDoing } from '../moderator/sprint_planning/taskDoing';
import { taskDelete } from '../moderator/sprint_planning/taskDelete';
import { sprintHandler } from '../moderator/sprint_planning/sprintHandler';
import { sprintTaskHandler } from '../moderator/sprint_planning/sprintTaskHandler';
import { getReview } from '../moderator/retrospective/getReview';
import { makeSummary } from '../moderator/sprint_planning/makeSummary';

export enum MessageOrigin {
    channels,
    ims,
    groups,
    mpim,
    unknown
}

export interface ChannelType {
    endpoint: string;
    data: string;
    origin: MessageOrigin;
}

export interface MessageEventType {
    type: string;
    channel: string;
    user: string;
    text: string;
    ts: string;
    origin: MessageOrigin;
}

interface IMessageHandler {
    matches: string[];
    handler(event: MessageEventType): Promise<void>;
}

export const channelTypes: ChannelType[] = [
    { endpoint: 'channels', data: 'channels', origin: MessageOrigin.channels },
    { endpoint: 'im', data: 'ims', origin: MessageOrigin.ims },
    { endpoint: 'mpim', data: 'groups', origin: MessageOrigin.mpim },
    { endpoint: 'groups', data: 'groups', origin: MessageOrigin.groups }
];

export async function isMemberOf(
    channelId: string,
    channelType: ChannelType,
    token = process.env.SLACK_BOT_USER_TOKEN
): Promise<boolean> {
    const url = `https://slack.com/api/${channelType.endpoint}.list`;
    return axios
        .get(url, {
            params: {
                token
            }
        })
        .then(response => {
            if (!response.data.ok) return false;
            const channel = response.data[channelType.data].find(
                (channel: any) => channel.id === channelId
            );
            return (
                !!channel &&
                (channelType.endpoint === 'channels' ? channel.is_member : true)
            );
        });
}

export function messageHandler(event: MessageEventType | any) {
    const eventHandlers: IMessageHandler[] = [
        {
            matches: ['/retrospective_done', '/retrospective'],
            handler: askHandler
        },
        {
            matches: ['/task_doing'],
            handler: taskDoing
        },
        {
            matches: ['/task_done'],
            handler: commandDone
        },
        {
            matches: ['/task_todo'],
            handler: taskRetrievalHandler
        },
        {
            matches: ['/sprint_create', '/sprint_start'],
            handler: sprintHandler
        },
        {
            matches: ['/sprint_task_plan'],
            handler: sprintTaskHandler
        },
        {
            matches: ['/get_retrospective'],
            handler: getReview
        },
        {
            matches: ['/summarize_planning'],
            handler: makeSummary
        },
        {
            matches: ['/task_backlog'],
            handler: retrieveBacklogHandler
        },
        {
            matches: ['/task_add'],
            handler: addTask
        },
        {
            matches: ['/task_assign'],
            handler: assignHandler
        },
        {
            matches: ['/task_finish'],
            handler: commandFinish
        },
        {
            matches: ['/task_delete'],
            handler: taskDelete
        },
        {
            matches: ['/sprint_review_red'],
            handler: redSprintReview,
        },
        {
            matches: ['/sprint_review_green'],
            handler: greenSprintReview,
        },
        {
            matches: ['/sprint_review_show'],
            handler: showSprintReview,
        },
    ];

    if (event.text !== undefined) {
        eventHandlers.forEach(eventHandler => {
            if (
                eventHandler.matches.some(command => event.text.startsWith(command))
            ) {
                eventHandler.handler(event);
            }
        });
    }

    return event;
}

export async function getSlackAccessToken(code: string): Promise<Object> {
    const res = await axios.get('https://slack.com/api/oauth.access', {
        params: {
            client_id: process.env.SLACK_CLIENT_ID,
            client_secret: process.env.SLACK_CLIENT_SECRET,
            code: code
        }
    });

    return res.data;
}
