import moxios from 'moxios';
import {
    channelTypes,
    isMemberOf,
    getSlackAccessToken,
    messageHandler,
    MessageEventType,
    MessageOrigin
} from '../utils';

beforeEach(() => {
    moxios.install();
});

afterEach(() => {
    moxios.uninstall();
});

const slackApiRegEx = /https\:\/\/slack\.com\/api\//;
const dotListRegEx = /\.list.*/;

channelTypes.forEach(channelType => {
    const urlRegEx = new RegExp(
        slackApiRegEx.source + channelType.endpoint + dotListRegEx.source
    );

    test(`isMemberOf ${channelType.endpoint} returns true`, async () => {
        const channelId = 'C1H9RESGL';
        const data = {
            ok: true,
            [channelType.data]: [{ id: channelId, is_member: true }]
        };

        moxios.stubRequest(urlRegEx, {
            status: 200,
            response: data
        });

        expect(await isMemberOf(channelId, channelType)).toBe(true);
    });

    test(`isMemberOf ${channelType.endpoint} returns false ok`, async () => {
        const channelId = 'C1H9RESGL';
        const data = {
            ok: true,
            [channelType.data]:
                channelType.endpoint === 'channels'
                    ? [{ id: channelId, is_member: false }]
                    : []
        };

        moxios.stubRequest(urlRegEx, {
            status: 200,
            response: data
        });

        expect(await isMemberOf(channelId, channelType)).toBe(false);
    });

    test(`isMemberOf ${
        channelType.endpoint
        } returns false not ok`, async () => {
            const channelId = 'C1H9RESGL';
            const data = { ok: false };

            moxios.stubRequest(urlRegEx, {
                status: 200,
                response: data
            });

            expect(await isMemberOf(channelId, channelType)).toBe(false);
        });
});

const SLACK_OAUTH_URL_REGEX = /https\:\/\/slack\.com\/api\/oauth\.access.*/;

test('getSlackAccessToken success', async () => {
    const code =
        '000000000000.000000000000.ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff';
    const data = {
        access_token: 'xoxp-XXXXXXXX-XXXXXXXX-XXXXX',
        scope: 'incoming-webhook,commands,bot',
        team_name: 'Team Installing Your Hook',
        team_id: 'XXXXXXXXXX',
        incoming_webhook: {
            url: 'https://hooks.slack.com/TXXXXX/BXXXXX/XXXXXXXXXX',
            channel: '#channel-it-will-post-to',
            configuration_url: 'https://teamname.slack.com/services/BXXXXX'
        },
        bot: {
            bot_user_id: 'UTTTTTTTTTTR',
            bot_access_token: 'xoxb-XXXXXXXXXXXX-TTTTTTTTTTTTTT'
        }
    };

    moxios.stubRequest(SLACK_OAUTH_URL_REGEX, {
        status: 200,
        response: data
    });

    expect(await getSlackAccessToken(code)).toBe(data);
});

test('getSlackAccessToken bad request', async () => {
    const code =
        '000000000000.000000000000.ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff';

    moxios.stubRequest(SLACK_OAUTH_URL_REGEX, {
        status: 502,
        response: {}
    });

    await expect(
        getSlackAccessToken(code)
    ).rejects.toThrowErrorMatchingSnapshot();
});

describe('messageHandler', async () => {
    const baseEvent: MessageEventType = {
        type: 'message',
        channel: 'C2147483705',
        user: 'U2147483697',
        text: 'Hello world',
        ts: '1355517523.000005',
        origin: MessageOrigin.channels
    };

    it('exists', () => {
        expect(messageHandler(baseEvent)).toBe(baseEvent);
    });

    it('calls askHandler on /retrospective', () => {
        const askHandlerModule = require('../../moderator/retrospective/askHandler');
        const handlerMock = jest.fn();
        askHandlerModule.askHandler = handlerMock;
        const event = {
            ...baseEvent,
            text: '/retrospective'
        };
        expect(messageHandler(event)).toBe(event);
        expect(handlerMock).toHaveBeenCalledWith(event);
    });

    it('calls assignHandler on /task_assign', () => {
        const assignHandlerModule = require('../../moderator/sprint_planning/assignHandler');
        const handlerMock = jest.fn();
        assignHandlerModule.assignHandler = handlerMock;
        const event = {
            ...baseEvent,
            text: '/task_assign'
        };
        expect(messageHandler(event)).toBe(event);
        expect(handlerMock).toHaveBeenCalledWith(event);
    });

    it('calls makeSummary on /summarize_planning', () => {
        const makeSummaryModule = require('../../moderator/sprint_planning/makeSummary');
        const handlerMock = jest.fn();
        makeSummaryModule.makeSummary = handlerMock;
        const event = {
            ...baseEvent,
            text: '/summarize_planning'
        };
        expect(messageHandler(event)).toBe(event);
        expect(handlerMock).toHaveBeenCalledWith(event);
    });

    it('calls askHandler on /retrospective_done', () => {
        const askHandlerModule = require('../../moderator/retrospective/askHandler');
        const handlerMock = jest.fn();
        askHandlerModule.askHandler = handlerMock;
        const event = {
            ...baseEvent,
            text: '/retrospective_done'
        };
        expect(messageHandler(event)).toBe(event);
        expect(handlerMock).toHaveBeenCalledWith(event);
    });

    it('calls commandFinish on /task_finish', () => {
        const commandFinishModule = require('../../moderator/meeting/command-finish');
        const handlerMock = jest.fn();
        commandFinishModule.commandFinish = handlerMock;
        const event = {
            ...baseEvent,
            text: '/task_finish'
        };
        expect(messageHandler(event)).toBe(event);
        expect(handlerMock).toHaveBeenCalledWith(event);
    });

    it('calls addTask on /task_add', () => {
        const addTaskModule = require('../../moderator/sprint_planning/addTask');
        const handlerMock = jest.fn();
        addTaskModule.addTask = handlerMock;
        const event = {
            ...baseEvent,
            text: '/task_add buat website 13'
        };
        expect(messageHandler(event)).toBe(event);
        expect(handlerMock).toHaveBeenCalledWith(event);
    });

    it('calls taskRetrievalHandler on /task_todo', async () => {
        const taskRetrievalHandlerModule = require('../../moderator/sprint_planning/taskRetrievalHandler');
        const handlerMock = jest.fn(() => '');
        taskRetrievalHandlerModule.taskRetrievalHandler = handlerMock;
        const event = {
            ...baseEvent,
            text: '/task_todo'
        };
        expect(messageHandler(event)).toBe(event);
        expect(handlerMock).toHaveBeenCalledWith(event);
    });

    it('calls taskDoing on /task_doing', () => {
        const taskDoingModule = require('../../moderator/sprint_planning/taskDoing');
        const handlerMock = jest.fn();
        taskDoingModule.taskDoing = handlerMock;
        const event = {
            ...baseEvent,
            text: '/task_doing'
        };
        expect(messageHandler(event)).toBe(event);
        expect(handlerMock).toHaveBeenCalledWith(event);
    });

    it('calls commandDone on /task_done', () => {
        const commandDoneModule = require('../../moderator/meeting/command-done');
        const handlerMock = jest.fn();
        commandDoneModule.commandDone = handlerMock;
        const event = {
            ...baseEvent,
            text: '/task_done'
        };
        expect(messageHandler(event)).toBe(event);
        expect(handlerMock).toHaveBeenCalledWith(event);
    });

    it('calls sprint_planning/sprintHandler on /sprint_create', () => {
        const handlerModule = require('../../moderator/sprint_planning/sprintHandler');
        const handlerMock = jest.fn();
        handlerModule.sprintHandler = handlerMock;
        const event = {
            ...baseEvent,
            text: '/sprint_create'
        };
        expect(messageHandler(event)).toBe(event);
        expect(handlerMock).toHaveBeenCalledWith(event);
    });

    it('calls sprint_planning/sprintHandler on /sprint_start', () => {
        const handlerModule = require('../../moderator/sprint_planning/sprintHandler');
        const handlerMock = jest.fn();
        handlerModule.sprintHandler = handlerMock;
        const event = {
            ...baseEvent,
            text: '/sprint_start'
        };
        expect(messageHandler(event)).toBe(event);
        expect(handlerMock).toHaveBeenCalledWith(event);
    });

    it('calls sprint_planning/sprintTaskHandler on /sprint_task_plan', () => {
        const handlerModule = require('../../moderator/sprint_planning/sprintTaskHandler');
        const handlerMock = jest.fn();
        handlerModule.sprintTaskHandler = handlerMock;
        const event = {
            ...baseEvent,
            text: '/sprint_task_plan'
        };
        expect(messageHandler(event)).toBe(event);
        expect(handlerMock).toHaveBeenCalledWith(event);
    });

    it('calls getReview on /get_retrospective', () => {
        const commandGetReview = require('../../moderator/retrospective/getReview');
        const handlerMock = jest.fn();
        commandGetReview.getReview = handlerMock;
        const event = {
            ...baseEvent,
            text: '/get_retrospective'

        };
        expect(messageHandler(event)).toBe(event);
        expect(handlerMock).toHaveBeenCalledWith(event);
    });

    it('calls retrive on /task_doing', () => {
        const handlerModule = require('../../moderator/sprint_planning/taskRetrievalHandler');
        const handlerMock = jest.fn();
        handlerModule.retrieveBacklogHandler = handlerMock;
        const event = {
            ...baseEvent,
            text: '/task_backlog'
        };
        expect(messageHandler(event)).toBe(event);
        expect(handlerMock).toHaveBeenCalledWith(event);
    });

    it('calls taskDelete on /task_delete', () => {
        const handlerModule = require('../../moderator/sprint_planning/taskDelete');
        const handlerMock = jest.fn();
        handlerModule.taskDelete = handlerMock;
        const event = {
            ...baseEvent,
            text: '/task_delete'
        };
        expect(messageHandler(event)).toBe(event);
        expect(handlerMock).toHaveBeenCalledWith(event);
    });

    it('calls redSprintReview on /sprint_review_red', () => {
        const sprintReviewModule = require('../../moderator/review/sprintReview');
        const handlerMock = jest.fn();
        sprintReviewModule.redSprintReview = handlerMock;
        const event = {
            ...baseEvent,
            text: '/sprint_review_red wowowow'
        };
        expect(messageHandler(event)).toBe(event);
        expect(handlerMock).toBeCalledWith(event);
    });

    it('calls greenSprintReview on /sprint_review_green', () => {
        const sprintReviewModule = require('../../moderator/review/sprintReview');
        const handlerMock = jest.fn();
        sprintReviewModule.greenSprintReview = handlerMock;
        const event = {
            ...baseEvent,
            text: '/sprint_review_green wowowow'
        };
        expect(messageHandler(event)).toBe(event);
        expect(handlerMock).toBeCalledWith(event);
    });

    it('calls showSprintReview on /sprint_review_show', () => {
        const sprintReviewModule = require('../../moderator/review/sprintReview');
        const handlerMock = jest.fn();
        sprintReviewModule.showSprintReview = handlerMock;
        const event = {
            ...baseEvent,
            text: '/sprint_review_show '
        };
        expect(messageHandler(event)).toBe(event);
        expect(handlerMock).toBeCalledWith(event);
    });

    it('ignores event with no texts', () => {
        const event = {
            ...baseEvent,
            text: undefined,
        };
        expect(messageHandler(event)).toBe(event);
    });
});
