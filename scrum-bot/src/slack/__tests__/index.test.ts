import moxios from 'moxios';
import {
    openConversation,
    sendDirectMessage,
    sendMessageToChannel,
    MessageDeliveryStatus
} from '../index';

beforeEach(() => {
    moxios.install();
});

afterEach(() => {
    moxios.uninstall();
});

const OPEN_CONVERSATION_POST_REGEX = /https\:\/\/slack\.com\/api\/conversations\.open.*/;

test('openConversation returns channel Id', async () => {
    const channel = 'U96GP7N8N';
    const data = { ok: true, channel: { id: 'D9L7KJPTR' } };

    moxios.stubRequest(OPEN_CONVERSATION_POST_REGEX, {
        status: 200,
        response: data
    });

    expect(await openConversation(channel)).toBe('D9L7KJPTR');
});

test('openConversation returns false', async () => {
    const channel = 'C1H999SGL';
    const data = { ok: false };

    moxios.stubRequest(OPEN_CONVERSATION_POST_REGEX, {
        status: 200,
        response: data
    });

    expect(await openConversation(channel)).toBe(false);
});

const SEND_DIRECT_MESSAGE_POST_REGEX = /https\:\/\/slack\.com\/api\/chat\.postMessage.*/;

test('sendDirectMessage returns Message Sent', async () => {
    const text = 'Hello World';
    const channel = 'U96GP7N8N';
    const data = { ok: true };

    moxios.stubRequest(SEND_DIRECT_MESSAGE_POST_REGEX, {
        status: 200,
        response: data
    });

    expect(await sendDirectMessage(channel, text)).toBe(
        MessageDeliveryStatus.SENT
    );
});

test('sendDirectMessage returns Message Not Sent', async () => {
    const text = 'Hello World';
    const channel = 'C1H999SGL';
    const data = { ok: false };

    moxios.stubRequest(SEND_DIRECT_MESSAGE_POST_REGEX, {
        status: 200,
        response: data
    });

    expect(await sendDirectMessage(channel, text)).toBe(
        MessageDeliveryStatus.FAILED
    );
});

const SEND_CHANNEL_MESSAGE_POST_REGEX = /https\:\/\/slack\.com\/api\/chat\.postMessage.*/;

test('sendMessageToChannel returns Message Sent', async () => {
    const channel = 'C966K4XBR';
    const text = 'Hello World';
    const data = { ok: true };

    moxios.stubRequest(SEND_CHANNEL_MESSAGE_POST_REGEX, {
        status: 200,
        response: data
    });

    expect(await sendMessageToChannel(channel, text)).toBe(MessageDeliveryStatus.SENT);
});

test('sendMessageToChannel returns Message Pending', async () => {
    const channel = 'C1H9RESGL';
    const text = 'Hello World';
    const data = { ok: false };

    moxios.stubRequest(SEND_CHANNEL_MESSAGE_POST_REGEX, {
        status: 200,
        response: data
    });

    expect(await sendMessageToChannel(channel, text)).toBe(MessageDeliveryStatus.FAILED);
});
