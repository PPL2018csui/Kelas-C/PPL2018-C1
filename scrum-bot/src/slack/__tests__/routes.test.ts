import Koa from 'koa';
import request from 'supertest';
import bodyParser from 'koa-bodyparser';
import { router } from '../routes';
import { channelTypes, MessageEventType, MessageOrigin } from '../utils';

let app: Koa;

beforeAll(async () => {
    require('dotenv').config();
    app = new Koa();
    app.use(bodyParser());
    app.use(router.routes());
});

test('Events request url authorized access', async () => {
    const response = await request(app.callback())
        .post('/slack/events')
        .send({ token: process.env.SLACK_VERIFICATION_TOKEN });
    expect(response.status).toBe(200);
});

test('Events request url unauthorized access', async () => {
    const unauthorizedToken = '';
    const response = await request(app.callback())
        .post('/slack/events')
        .send({ token: unauthorizedToken });
    expect(response.status).toBe(401);
});

test('Events request url passes slack challenge', async () => {
    const urlVerificationEvent = {
        token: process.env.SLACK_VERIFICATION_TOKEN,
        challenge: 'some_challenge_string',
        type: 'url_verification'
    };
    const response = await request(app.callback())
        .post('/slack/events')
        .set('Content-Type', 'application/json')
        .send(urlVerificationEvent);
    expect(response.status).toBe(200);
    expect(response.body.challenge).toBe(urlVerificationEvent.challenge);
});

test(`Events request url message not from any member of`, async () => {
    const mockIsMemberOf = jest.fn();
    const utils = require('../utils');

    mockIsMemberOf.mockReturnValue(false);
    utils.isMemberOf = mockIsMemberOf;
    utils.messageHandler = jest.fn();

    const messageEvent: MessageEventType = {
        type: 'message',
        channel: 'C2147483705',
        user: 'U2147483697',
        text: 'Hello world',
        ts: '1355517523.000005',
        origin: MessageOrigin.unknown,
    };

    const slackEvent = {
        token: process.env.SLACK_VERIFICATION_TOKEN,
        type: 'event_callback',
        event: messageEvent
    };
    const response = await request(app.callback())
        .post('/slack/events')
        .set('Content-Type', 'application/json')
        .send(slackEvent);
    expect(response.status).toBe(200);
    expect(utils.messageHandler).toBeCalledWith(messageEvent);
});

channelTypes.forEach(channelType => {
    test(`Events request url message from ${
        channelType.endpoint
        } member of`, async () => {
            const mockIsMemberOf = jest.fn((channelId, typeArg) => {
                return typeArg.endpoint === channelType.endpoint;
            });
            const utils = require('../utils');

            utils.isMemberOf = mockIsMemberOf;
            utils.messageHandler = jest.fn();

            const messageEvent: MessageEventType = {
                type: 'message',
                channel: 'C2147483705',
                user: 'U2147483697',
                text: 'Hello world',
                ts: '1355517523.000005',
                origin: channelType.origin,
            };

            const slackEvent = {
                token: process.env.SLACK_VERIFICATION_TOKEN,
                type: 'event_callback',
                event: messageEvent
            };
            const response = await request(app.callback())
                .post('/slack/events')
                .set('Content-Type', 'application/json')
                .send(slackEvent);
            expect(response.status).toBe(200);
            expect(utils.messageHandler).toBeCalledWith(messageEvent);
        });
});

test('Commands request url authorized access', async () => {
    const response = await request(app.callback())
        .post('/slack/commands/some_slash_command')
        .send({ token: process.env.SLACK_VERIFICATION_TOKEN });
    expect(response.status).toBe(200);
});

test('Commands request url unauthorized access', async () => {
    const unauthorizedToken = '';
    const response = await request(app.callback())
        .post('/slack/commands/some_slash_command')
        .send({ token: unauthorizedToken });
    expect(response.status).toBe(401);
});

test('Slack OAuth denies authorization', async () => {
    const response = await request(app.callback()).get('/slack/oauth?error=access_denied');
    expect(response.status).toBe(302);
});

test('Slack OAuth process Access Token', async () => {
    const getSlackAccessToken = jest.fn();
    const data = {
        'access_token': 'xoxp-XXXXXXXX - XXXXXXXX - XXXXX'
    };

    getSlackAccessToken.mockReturnValue(data);

    const utils = require('../utils');
    utils.getSlackAccessToken = getSlackAccessToken;

    const response = await request(app.callback()).get('/slack/oauth?code=000000000000.000000000000.ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff');
    expect(response.status).toBe(200);
    expect(response.body).toEqual(data);
});

test('OAuth url works', async () => {
    const response = await request(app.callback()).get('/slack/oauth');
    expect(response.status).toBe(200);
});
